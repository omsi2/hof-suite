(C) 2019 Lukas G.

# English

You are allowed to use, upload and edit this software, under the following conditions:
- Attribution of the author
- Linking to https://forum.omnibussimulator.de/forum/index.php?thread/40888-hof-suite/ and https://reboot.omsi-webdisk.de/file/773-hof-suite-en-de/
- Disclosure of the changed source code

Commercial use or reselling of the application is forbidden! The HOF files written with this application are free to use including commercial content.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Deutsch

Es ist erlaubt, diese Anwendung zu nutzen, erneut hochzuladen und den Quellcode zu verändern. Folgende Bedingungen gelten:
- Namensnennung des Autors
- Verlinkung auf https://forum.omnibussimulator.de/forum/index.php?thread/40888-hof-suite/ und https://reboot.omsi-webdisk.de/file/773-hof-suite-en-de/
- Offenlegung des veränderten Quellcodes

Kommerzielle Benutzung oder Verbreitung dieser Software ist verboten! Die mit diesem Programm erstellten HOF-Dateien stehen zur freien (auch kommerziellen) Verfügung; eine Namensnennung des Programms ist zwar erwünscht, aber nicht notwendig.

DIE SOFTWARE WIRD OHNE JEDE AUSDRÜCKLICHE ODER IMPLIZIERTE GARANTIE BEREITGESTELLT, EINSCHLIEẞLICH DER GARANTIE ZUR BENUTZUNG FÜR DEN VORGESEHENEN ODER EINEM BESTIMMTEN ZWECK SOWIE JEGLICHER RECHTSVERLETZUNG, JEDOCH NICHT DARAUF BESCHRÄNKT. IN KEINEM FALL SIND DIE AUTOREN ODER COPYRIGHTINHABER FÜR JEGLICHEN SCHADEN ODER SONSTIGE ANSPRÜCHE HAFTBAR ZU MACHEN, OB INFOLGE DER ERFÜLLUNG EINES VERTRAGES, EINES DELIKTES ODER ANDERS IM ZUSAMMENHANG MIT DER SOFTWARE ODER SONSTIGER VERWENDUNG DER SOFTWARE ENTSTANDEN.