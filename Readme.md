 [webdisk]: <https://reboot.omsi-webdisk.de/community/thread/480>
 [hofsuite]: <https://reboot.omsi-webdisk.de/file/773>
 [visualstudio]: <https://visualstudio.microsoft.com>
 [gitsetup]: <https://rogerdudler.github.io/git-guide/index.de.html>

# Changelog
* **Version 1.0**
    * Release
* **Version 1.1**
    * Export von hof-Dateien in mehrere Ordner gleichzeitig
* **Version 1.2**
    * Aktualisierung des Handbuchs
    * Verschiedene Fixe
* **Version 1.3**
    * Unterstützung für StationLinks
* **Version 1.4**
    * Verbessertes User Interface
    * Eigene Definierung von Strings mit einem Template
* **Version 1.5**
    * Große Überarbeitung des Codes
    * OpenSource-Veröffentlichung
    * .net Framework 4.7
    * Performance-Verbesserungen

# Über dieses Repository

Vorab: In der [OMSI WebDisk][webdisk] befinden sich weitere Informationen zu meinen Projekten. Zur HOF Suite an sich geht es [hier][hofsuite].

Dies ist das Repository der HOF Suite. Hier kannst Du nach Belieben herumstöbern, Dateien herunterladen oder gleich das ganze Archiv herunterladen (wobei ich dann doch lieber den Download von der [OMSI WebDisk][hofsuite] empfehle). Falls doch Bedarf besteht: über den rechten oberen Button lässt sich der aktuell ausgewählte Ordner packen und herunterladen.

## Mitmachen

Die HOF Suite ist OpenSource, das heißt, jeder kann prinzipiell mitmachen. Du brauchst lediglich ein [eingerichtetes Git][gitsetup] und eine eingerichtete [Microsoft VisualStudio IDE *(Community)*][visualstudio]. Bei der Installation von letzterem wird nur das *.net Framework 4.7**- und das *C# Desktop*-Pack benötigt. Das Projekt kann auch über VisualStudio importiert werden.

Nach dem Öffnen der Projektmappe kann das Programm mit dem grünen Start-Button kompiliert und ausgeführt werden, es sollten alle Abhängigkeiten gelöst und alle notwendigen Dateien kopiert werden.