﻿namespace Hofsuite.Core
{
    class Keywords
    {
        public const string HEADER_DE = "Diese Datei wurde mit der HOF Suite von Rumpelhans geschrieben.";
        public const string HEADER_EN = "This file was written with HOF Suite by Rumpelhans.";

        public const string SUITE_DIRECTORIES = "[HOF_SUITE_DIRECTORIES]";
        public const string SUITE_DIRECTORIES_END = "[HOF_SUITE_DIRECTORIES_END]";

        public const string END = "[end]";

        public const string NAME = "[name]";
        public const string SERVICETRIP = "[servicetrip]";
        public const string GLOBAL_STRINGS = "[global_strings]";

        public const string COUNT_TERMINUS = "stringcount_terminus";
        public const string ALLEXIT = "{ALLEX}";
        public const string TERMINUS = "[addterminus]";
        public const string TERMINUS_ALLEXIT = "[addterminus_allexit]";
        public const string TERMINUS_LIST = "[addterminus_list]";

        public const string COUNT_BUSSTOP = "stringcount_busstop";
        public const string BUSSTOP = "[addbusstop]";
        public const string BUSSTOP_LIST = "[addbusstop_list]";

        public const string ROUTE = "[infosystem_trip]";
        public const string ROUTE_STOPS = "[infosystem_busstop_list]";

        public const string OMSI1_SEPARATOR = ".........................";

        // custom strings
        public const string CUSTOMSTRINGS_TERMINUS = "[TerminusString]";
        public const string CUSTOMSTRINGS_BUSSTOP = "[BusstopString]";

        // trips
        public const string TRIP = "[trip]";
        public const string TRIP_BUSSTOP = "[busstop]";
        public const string TRIP_STATION = "[station]";
        public const string TRIP_STATION_TYPE2 = "[station_typ2]";
    }
}
