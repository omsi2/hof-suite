﻿using System.Drawing;
using System.Windows.Forms;

namespace Hofsuite.Core.Structs
{
    class Map : ListViewItem
    {
        public Map(string path, string displayname, Image image)
        {
            Path = path;
            Displayname = displayname;
            Text = displayname;
            Image = image;
            ToolTipText = path;
        }

        public string Path { get; }

        public string Displayname { get; }

        public Image Image { get; }

        public override string ToString()
        {
            return Displayname;
        }
    }
}
