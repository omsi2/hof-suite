﻿using System.IO;
using System.Text;

namespace Hofsuite.Core.Structs
{
    class Reader : StreamReader
    {
        public Reader(string path) : base(path, Encoding.Default)
        {
        }

        static readonly char[] trimChars = { '\t', ' ' };

        public int CurrentLine { private set; get; } = 0;

        public override string ReadLine()
        {
            CurrentLine++;
            string line = base.ReadLine();

            return line?.TrimEnd(trimChars);
        }
    }
}
