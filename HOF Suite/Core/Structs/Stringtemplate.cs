﻿namespace Hofsuite.Core.Structs
{
    struct Stringtemplate : IStringtemplate
    {
        public Stringtemplate(string name_DE, string name_EN, string mask = "")
        {
            Name_DE = name_DE;
            Name_EN = name_EN;
            Mask = mask;
        }

        public string Name_DE { get; }

        public string Name_EN { get; }

        public string Mask { get; }
    }

    struct StringtemplateFile : IStringtemplate
    {
        public StringtemplateFile(string name_DE, string name_EN, ImageType imageType)
        {
            Name_DE = name_DE;
            Name_EN = name_EN;
            Mask = "";
            ImageType = imageType;
        }

        public string Name_DE { get; }

        public string Name_EN { get; }

        public string Mask { get; }

        public ImageType ImageType { get; }
    }
}
