﻿using System.Collections.Generic;

namespace Hofsuite.Core.Structs
{
    class Hof
    {
        public Hof()
        {
        }

        public Hof(HofSettings settings, List<Destination> destinations, List<Stop> stops, List<Route> routes)
        {
            Settings = settings ?? new HofSettings();
            Destinations = destinations ?? new List<Destination>();
            Stops = stops ?? new List<Stop>();
            Routes = routes ?? new List<Route>();
        }

        public HofSettings Settings { get; } = new HofSettings();

        public List<Destination> Destinations { get; } = new List<Destination>();

        public List<Stop> Stops { get; } = new List<Stop>();

        public List<Route> Routes { get; } = new List<Route>();
    }
}
