﻿using Hofsuite.Core.Business;

using System.Collections.Generic;

namespace Hofsuite.Core.Structs
{
    class HofSettings : IWritable
    {
        public HofSettings()
        {
        }

        public HofSettings(string name, Destination servicetrip, string[] globalStrings, HashSet<string> vehicleDirectories)
        {
            Name = name;
            Servicetrip = servicetrip;
            GlobalStrings = globalStrings ?? new string[6];
            VehicleDirectories = vehicleDirectories ?? new HashSet<string>();
        }

        public string Name { get; set; }

        public Destination Servicetrip { get; set; }

        string[] GlobalStrings { get; } = new string[6];

        public string GetGlobalString(int index) => GlobalStrings[index];
        public string GetGlobalString(GlobalStringType type) => GlobalStrings[(int)type];

        public void SetGlobalString(GlobalStringType type, string value) => GlobalStrings[(int)type] = value;

        public HashSet<string> VehicleDirectories { get; } = new HashSet<string>();

        public void Write(Writer writer)
        {
            // Write vehicle count
            if (VehicleDirectories.Count > 0)
            {
                writer.WriteLine(Keywords.SUITE_DIRECTORIES);
                foreach (string vehicleDirectory in VehicleDirectories) writer.WriteLine(vehicleDirectory);
                writer.WriteLine(Keywords.SUITE_DIRECTORIES_END);
                writer.WriteLine();
            }

            // Write name
            writer.WriteLine(Keywords.NAME);
            writer.WriteLine(Name);
            writer.WriteLine();

            // Write servicetrip
            writer.WriteLine(Keywords.SERVICETRIP);
            writer.WriteLine(Servicetrip == null ? "" : Servicetrip.Name);
            writer.WriteLine();

            // Write global strings
            writer.WriteLine(Keywords.GLOBAL_STRINGS);
            writer.WriteLine(GlobalStrings.Length);
            foreach (var s in GlobalStrings) writer.WriteLine(s?.Trim());
            writer.WriteLine();

            // Write stringcount terminus
            writer.WriteLine(Keywords.COUNT_TERMINUS);
            writer.WriteLine(StringtemplateLoader.GetDestinationStringcount());
            writer.WriteLine();

            // Write stringcount busstop
            writer.WriteLine(Keywords.COUNT_BUSSTOP);
            writer.WriteLine(StringtemplateLoader.GetStopStringcount());
            writer.WriteLine();
        }
    }
}
