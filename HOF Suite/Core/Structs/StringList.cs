﻿using System.Collections.Generic;
using System.Linq;

namespace Hofsuite.Core.Structs
{
    /// <summary>
    /// Represents a list of Strings for Destinations and Stops
    /// </summary>
    class StringList : SortedList<int, string>, IWritable
    {
        public StringList() : base()
        {
        }

        public StringList(StringList other) : base(other)
        {
        }

        public StringList(int capacity) : base(capacity)
        {
        }

        public StringList(IComparer<int> comparer) : base(comparer)
        {
        }

        public StringList(int capacity, IComparer<int> comparer) : base(capacity, comparer)
        {
        }

        public StringList(IDictionary<int, string> dictionary) : base(dictionary)
        {
        }

        public StringList(IDictionary<int, string> dictionary, IComparer<int> comparer) : base(dictionary, comparer)
        {
        }

        public void SetString(int key, string value)
        {
            if (value.IsEmpty() && ContainsKey(key)) Remove(key);
            else if (ContainsKey(key)) this[key] = value;
            else Add(key, value);
        }

        public string GetString(int key)
        {
            if (ContainsKey(key)) return this[key];
            else return "";
        }

        public void Write(Writer writer)
        {
            if (Count == 0) return;
            for (int i = 0; i <= Keys.Max(); i++)
            {
                writer.Write("\t");
                writer.Write(GetString(i));
            }
        }
    }
}
