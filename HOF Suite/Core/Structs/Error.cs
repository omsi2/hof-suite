﻿using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Hofsuite.Core.Structs
{
    class Error : ListViewItem
    {
        public Error(string file, int lineNumber, string message, bool isException)
        {
            if (isException) ForeColor = Color.Firebrick;
            Text = Path.GetFileName(file);
            File = file;
            SubItems.Add(lineNumber.ToString());
            SubItems.Add(message);
        }

        public string File { get; }
    }
}
