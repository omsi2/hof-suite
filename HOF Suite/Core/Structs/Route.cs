﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hofsuite.Core.Structs
{
    class Route : Item, IComparable, IExtraComparable
    {
        public Route(string name = "", int line = 1, int tour = 1, Destination destination = null, List<Stop> stops = null)
        {
            Name = name;
            Line = line;
            Tour = tour;
            Destination = destination;
            Stops = stops ?? new List<Stop>();
        }

        public int Line { get; set; }

        public int Tour { get; set; }

        public Destination Destination { get; set; }

        public List<Stop> Stops { get; }

        public override string ToString()
        {
            string result = $"{Line}/{Tour}";
            if (Destination != null) result += $" [{Destination.Code}]";
            if (!Name.IsEmpty()) result += $" {Name}";
            return result;
        }

        public override void Write(Writer writer)
        {
            writer.WriteLine(Keywords.ROUTE);
            writer.Write(Line);
            writer.WriteLine(Tour.ToString().PadLeft(2, '0'));
            writer.WriteLine(Name);
            writer.WriteLine(Destination == null ? "0" : Destination.Code.ToString());
            writer.WriteLine(Line);
            writer.WriteLine();

            writer.WriteLine(Keywords.ROUTE_STOPS);
            writer.WriteLine(Stops.Count);
            foreach (var stop in Stops) writer.WriteLine(stop.Name);
            writer.WriteLine();
        }

        public static int FindNextTourNumber(int line, List<Route> routes)
        {
            routes = routes.FindAll(route => route.Line == line);
            return routes.Count == 0 ? 1 : routes.Max(route => route.Tour) + 1;
        }

        public override Item DuplicateItem() => new Route(Name, Line, Tour, Destination, new List<Stop>(Stops));

        public int CompareTo(object other) => string.Compare(
            Line.ToString().PadLeft(4, '0') + Tour.ToString().PadLeft(2, '0'),
            ((Route)other).Line.ToString().PadLeft(4, '0') + ((Route)other).Tour.ToString().PadLeft(2, '0'));

        public int CompareByName(object other) => Name.CompareTo(((Route)other).Name);
    }
}
