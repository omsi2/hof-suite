﻿using Hofsuite.Core.Structs;
using Hofsuite.Properties;

using System;
using System.Collections.Generic;

namespace Hofsuite.Core.Business
{
    class HofWriter : IHasErrors
    {
        public HofWriter(string filename, Hof hoffile)
        {
            Filename = filename;
            Hoffile = hoffile;
        }

        public List<Error> Errors { get; } = new List<Error>();

        public Hof Hoffile { get; }

        string Filename { get; }

        /// <summary>
		/// Save a .hof file
		/// </summary>
		public void WriteFile()
        {
            using (var writer = new Writer(Filename))
            {
                try
                {
                    // Write header and properties
                    WriteHeader(writer);
                    WriteProperties(writer);

                    // Write Termini
                    writer.WriteTitle("Termini");
                    writer.WriteLine(Keywords.TERMINUS_LIST);
                    foreach (Destination d in Hoffile.Destinations) WriteTerminus(writer, d);
                    writer.WriteLine(Keywords.END);
                    writer.WriteLine();

                    // Write Busstops
                    writer.WriteTitle("Busstops");
                    writer.WriteLine(Keywords.BUSSTOP_LIST);
                    foreach (Stop s in Hoffile.Stops) WriteBusstop(writer, s);
                    writer.WriteLine(Keywords.END);
                    writer.WriteLine();

                    // Write Routes
                    writer.WriteTitle("Routes");
                    foreach (Route r in Hoffile.Routes) WriteRoute(writer, r);
                }
                catch (Exception e)
                {
                    Errors.Add(new Error(
                        Filename,
                        writer.CurrentLine,
                        string.Format(Text.EXPORT_Exception, e.Message, e.TargetSite),
                        true));
                }
            }
        }

        /// <summary>
        /// Write header
        /// </summary>
        void WriteHeader(Writer writer)
        {
            try
            {
                writer.WriteLine(Keywords.HEADER_DE);
                writer.WriteLine(Keywords.HEADER_EN);
                writer.WriteLine();
            }
            catch (Exception e)
            {
                Errors.Add(new Error(
                    Filename,
                    writer.CurrentLine,
                    string.Format(Text.EXPORT_ExceptionHeader, e.Message, e.TargetSite),
                    true));
            }
        }

        /// <summary>
        /// Write properties (vehicle directories, global strings, stringcounts)
        /// </summary>
        void WriteProperties(Writer writer)
        {
            try
            {
                writer.WriteTitle("HOF File properties");
                Hoffile.Settings.Write(writer);
            }
            catch (Exception e)
            {
                Errors.Add(new Error(
                    Filename,
                    writer.CurrentLine,
                    string.Format(Text.EXPORT_ExceptionProperties, e.Message, e.TargetSite),
                    true));
            }
        }

        /// <summary>
        /// Write Terminus
        /// </summary>
        void WriteTerminus(Writer writer, Destination destination)
        {
            try
            {
                destination.Write(writer);
            }
            catch (Exception e)
            {
                Errors.Add(new Error(
                    Filename,
                    writer.CurrentLine,
                    string.Format(Text.EXPORT_ExceptionTerminus, destination.ToString(), e.Message, e.TargetSite),
                    true));
            }
        }

        /// <summary>
        /// Write Busstop
        /// </summary>
        void WriteBusstop(Writer writer, Stop stop)
        {
            try
            {
                stop.Write(writer);
            }
            catch (Exception e)
            {
                Errors.Add(new Error(
                    Filename,
                    writer.CurrentLine,
                    string.Format(Text.EXPORT_ExceptionBusstop, stop.ToString(), e.Message, e.TargetSite),
                    true));
            }
        }

        /// <summary>
        /// Write Route
        /// </summary>
        void WriteRoute(Writer writer, Route route)
        {
            try
            {
                route.Write(writer);
            }
            catch (Exception e)
            {
                Errors.Add(new Error(
                    Filename,
                    writer.CurrentLine,
                    string.Format(Text.EXPORT_ExceptionRoute, route.ToString(), e.Message, e.TargetSite),
                    true));
            }
        }
    }
}
