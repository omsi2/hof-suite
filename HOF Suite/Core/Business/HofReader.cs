﻿using Hofsuite.Core.Structs;
using Hofsuite.Properties;

using System;
using System.Collections.Generic;

namespace Hofsuite.Core.Business
{
    class HofReader : IHasErrors
    {
        public HofReader(string filename)
        {
            Filename = filename;
        }

        public List<Error> Errors { get; } = new List<Error>();

        public Hof NewHoffile { get; } = new Hof();

        string Filename { get; }

        // temporary variables
        int tempTerminusStringcount = 0;
        int tempBusstopStringcount = 0;
        string tempServicetrip = null;

        /// <summary>
		/// Open a .hof file
		/// </summary>
		public void ReadFile()
        {
            using (var reader = new Reader(Filename))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    try
                    {
                        switch (line)
                        {
                            // Switch the keywords

                            case Keywords.SUITE_DIRECTORIES: AddExportDirectories(reader); break;

                            case Keywords.NAME: SetName(reader); break;
                            case Keywords.SERVICETRIP: SetServicetrip(reader); break;
                            case Keywords.GLOBAL_STRINGS: SetGlobalStrings(reader); break;

                            case Keywords.COUNT_TERMINUS: SetStringcount(reader, StringType.Destination); break;
                            case Keywords.COUNT_BUSSTOP: SetStringcount(reader, StringType.Stop); break;

                            case Keywords.TERMINUS: AddTerminus(reader, false); break;
                            case Keywords.TERMINUS_ALLEXIT: AddTerminus(reader, true); break;
                            case Keywords.TERMINUS_LIST: AddTerminusList(reader); break;

                            case Keywords.BUSSTOP: AddBusstop(reader); break;
                            case Keywords.BUSSTOP_LIST: AddBusstopList(reader); break;

                            case Keywords.ROUTE: AddRoute(reader); break;
                        }
                    }
                    catch (Exception e)
                    {
                        Errors.Add(new Error(
                            Filename,
                            reader.CurrentLine,
                            string.Format(Text.IMPORT_Exception, e.Message, e.TargetSite),
                            true));
                    }
                }

                LinkServicetripTerminus(reader);
            }
        }

        // [name]
        void SetName(Reader reader)
        {
            NewHoffile.Settings.Name = reader.ReadLine();
        }

        // [servicetrip]
        void SetServicetrip(Reader reader)
        {
            tempServicetrip = reader.ReadLine();
        }

        // [global_strings]
        void SetGlobalStrings(Reader reader)
        {
            try
            {
                int count = int.Parse(reader.ReadLine());
                for (int i = 0; i < count; i++) NewHoffile.Settings.SetGlobalString((GlobalStringType)i, reader.ReadLine());
            }
            catch (Exception e)
            {
                if (e is FormatException)
                {
                    Errors.Add(new Error(
                        Filename,
                        reader.CurrentLine,
                        Text.IMPORT_GlobalStringsCounterNotValid,
                        false));
                }
                else if (e is ArgumentNullException)
                {
                    Errors.Add(new Error(
                        Filename,
                        reader.CurrentLine,
                        Text.IMPORT_GlobalStringsCounterEmpty,
                        false));
                }
                else
                {
                    Errors.Add(new Error(
                        Filename,
                        reader.CurrentLine,
                        string.Format(Text.IMPORT_GlobalStringsException, e.Message, e.TargetSite),
                        true));
                }
            }
        }

        // stringcount_terminus
        // stringcount_busstop
        void SetStringcount(Reader reader, StringType type)
        {
            try
            {
                string line = reader.ReadLine();
                if (type == StringType.Destination) tempTerminusStringcount = int.Parse(line);
                else if (type == StringType.Stop) tempBusstopStringcount = int.Parse(line);
                else throw new ArgumentException("Internal error: Enum is not valid!");
            }
            catch (Exception e)
            {
                if (e is FormatException)
                {
                    Errors.Add(new Error(
                        Filename,
                        reader.CurrentLine,
                        type == StringType.Destination ?
                            Text.IMPORT_StringcountTerminusNotValid : Text.IMPORT_StringcountBusstopNotValid,
                        false));
                }
                else if (e is ArgumentNullException)
                {
                    Errors.Add(new Error(
                        Filename,
                        reader.CurrentLine,
                        type == StringType.Destination ?
                            Text.IMPORT_StringcountTerminusEmpty : Text.IMPORT_StringcountBusstopEmpty,
                        false));
                }
                else
                {
                    Errors.Add(new Error(
                        Filename,
                        reader.CurrentLine,
                        string.Format(type == StringType.Destination ?
                            Text.IMPORT_StringcountTerminusException : Text.IMPORT_StringcountBusstopException,
                            e.Message, e.TargetSite),
                        false));
                }
            }
        }

        // [addterminus_list]
        void AddTerminusList(Reader reader)
        {
            string line;
            while ((line = reader.ReadLine()) != Keywords.END)
            {
                try
                {
                    string[] columns = line.Split('\t');
                    var currentTerminus = new Destination();

                    if (columns.Length < 3)
                    {
                        Errors.Add(new Error(
                            Filename,
                            reader.CurrentLine,
                            Text.IMPORT_TerminusListTooLittleTabs,
                            false));
                    }
                    for (int i = 0; i < columns.Length; i++)
                    {
                        switch (i)
                        {
                            case 0:
                                currentTerminus.Allexit = columns[i] != "";
                                break;

                            case 1:
                                try
                                {
                                    currentTerminus.Code = int.Parse(columns[i]);
                                }
                                catch (Exception)
                                {
                                    Errors.Add(new Error(
                                        Filename,
                                        reader.CurrentLine,
                                        string.Format(Text.IMPORT_TerminusCodeNotValid, currentTerminus.ToString()),
                                        false));
                                    currentTerminus.Code = 0;
                                }
                                break;

                            case 2:
                                currentTerminus.Name = columns[i];
                                break;

                            default:
                                currentTerminus.StringList.SetString(i - 3, columns[i]);
                                break;
                        }
                    }
                    NewHoffile.Destinations.Add(currentTerminus);
                }
                catch (Exception e)
                {
                    Errors.Add(new Error(
                        Filename,
                        reader.CurrentLine,
                        string.Format(Text.IMPORT_TerminusListException, e.Message, e.TargetSite),
                        true));
                }
            }
        }

        // [addbusstop_list]
        void AddBusstopList(Reader reader)
        {
            string line;
            while ((line = reader.ReadLine()) != Keywords.END)
            {
                try
                {
                    string[] columns = line.Split('\t');
                    var currentBusstop = new Stop();

                    if (columns.Length < 1)
                    {
                        Errors.Add(new Error(
                            Filename,
                            reader.CurrentLine,
                            Text.IMPORT_BusstopListLineEmpty,
                            false));
                        continue;
                    }
                    for (int i = 0; i < columns.Length; i++)
                    {
                        switch (i)
                        {
                            case 0:
                                currentBusstop.Name = columns[i];
                                break;

                            default:
                                currentBusstop.StringList.SetString(i - 1, columns[i]);
                                break;
                        }
                    }
                    NewHoffile.Stops.Add(currentBusstop);

                }
                catch (Exception e)
                {
                    Errors.Add(new Error(
                        Filename,
                        reader.CurrentLine,
                        string.Format(Text.IMPORT_BusstopListException, e.Message, e.TargetSite),
                        true));
                }
            }
        }

        // [infosystem_trip]
        // [infosystem_busstop_list]
        void AddRoute(Reader reader)
        {
            try
            {

                string[] lines = { reader.ReadLine(), reader.ReadLine(), reader.ReadLine(), reader.ReadLine() };

                // set line number
                if (!int.TryParse(lines[0].Substring(0, lines[0].Length - 2), out int lineNumber) &&
                    !int.TryParse(lines[3].Substring(0, lines[3].Length - 2), out lineNumber))
                {
                    lineNumber = 0;
                    Errors.Add(new Error(
                        Filename,
                        reader.CurrentLine,
                        Text.IMPORT_RouteLineNotValid,
                        false));
                }

                // set tour number
                if (!int.TryParse(lines[0].Substring(lines[0].Length - 2), out int tourNumber))
                {
                    tourNumber = Route.FindNextTourNumber(lineNumber, NewHoffile.Routes);
                    Errors.Add(new Error(
                        Filename,
                        reader.CurrentLine,
                        Text.IMPORT_RouteTourNotValid,
                        false));
                }

                // set name
                string routeName = lines[1];

                var currentRoute = new Route(routeName, lineNumber, tourNumber);

                // set destination
                if (int.TryParse(lines[2], out int destinationNumber))
                {
                    var destination = NewHoffile.Destinations.Find(x => x.Code == destinationNumber);
                    if (destination == null)
                    {
                        Errors.Add(new Error(
                            Filename,
                            reader.CurrentLine,
                            string.Format(Text.IMPORT_RouteTerminusNotFound, destinationNumber, currentRoute.ToString()),
                            false));
                    }
                    else
                    {
                        currentRoute.Destination = destination;
                    }
                }
                else
                {
                    destinationNumber = 0;
                    Errors.Add(new Error(
                        Filename,
                        reader.CurrentLine,
                        string.Format(Text.IMPORT_RouteTerminusCodeNotValid, currentRoute.ToString()),
                        false));
                }

                // find busstop-list-entry
                string line;
                while ((line = reader.ReadLine()) != Keywords.ROUTE_STOPS)
                {
                    if (line == null)
                    {
                        Errors.Add(new Error(
                            Filename,
                            reader.CurrentLine,
                            string.Format(Text.IMPORT_RouteNoBusstopListFound, currentRoute.ToString()),
                            false));

                        NewHoffile.Routes.Add(currentRoute);
                        return;
                    }
                    if (line == Keywords.ROUTE)
                    {
                        Errors.Add(new Error(
                            Filename,
                            reader.CurrentLine,
                            string.Format(Text.IMPORT_RouteNoBusstopListFound, currentRoute.ToString()),
                            false));

                        NewHoffile.Routes.Add(currentRoute);
                        AddRoute(reader);
                        return;
                    }
                }

                // get busstop count
                if (!int.TryParse(reader.ReadLine(), out int count))
                {
                    Errors.Add(new Error(
                        Filename,
                        reader.CurrentLine,
                        string.Format(Text.IMPORT_RouteBusstopCountNotValid, currentRoute.ToString()),
                        false));
                    return;
                }

                for (int i = 0; i < count; i++)
                {
                    string busstopName = reader.ReadLine();
                    var busstop = NewHoffile.Stops.Find(x => x.Name == busstopName);
                    if (busstop == null)
                    {
                        Errors.Add(new Error(
                            Filename,
                            reader.CurrentLine,
                            string.Format(Text.IMPORT_RouteBusstopNotFound, busstopName, currentRoute.ToString()),
                            false));
                    }
                    else
                    {
                        currentRoute.Stops.Add(busstop);
                    }

                }
                NewHoffile.Routes.Add(currentRoute);

            }
            catch (Exception e)
            {
                Errors.Add(new Error(
                    Filename,
                    reader.CurrentLine,
                    string.Format(Text.IMPORT_RouteException, e.Message, e.TargetSite),
                    true));
            }
        }

        // [addterminus_allexit]
        // [addterminus]
        void AddTerminus(Reader reader, bool allexit)
        {
            try
            {
                if (tempTerminusStringcount == 0)
                {
                    Errors.Add(new Error(
                        Filename,
                        reader.CurrentLine,
                        Text.IMPORT_TerminusStringcountNotFound,
                        false));
                }

                var currentTerminus = new Destination
                {
                    Allexit = allexit
                };
                if (!int.TryParse(reader.ReadLine(), out int code))
                {
                    Errors.Add(new Error(
                        Filename,
                        reader.CurrentLine,
                        string.Format(Text.IMPORT_TerminusCodeNotValid, currentTerminus.Name),
                        true));
                }
                else
                {
                    currentTerminus.Code = code;
                }

                currentTerminus.Name = reader.ReadLine();

                string line;
                for (int i = 0;
                    // continue if iterater is lower than stringcount OR  no stringcount was found
                    (tempTerminusStringcount == 0 || i < tempTerminusStringcount) &&
                    // AND ONLY if there is no separator
                    !(line = reader.ReadLine()).Contains(Keywords.OMSI1_SEPARATOR);
                    i++)
                {
                    currentTerminus.StringList.SetString(i, line);
                }
                NewHoffile.Destinations.Add(currentTerminus);
            }
            catch (Exception e)
            {
                Errors.Add(new Error(
                    Filename,
                    reader.CurrentLine,
                    string.Format(Text.IMPORT_TerminusException, e.Message, e.TargetSite),
                    true));
            }
        }

        // [addbusstop]
        void AddBusstop(Reader reader)
        {
            try
            {
                if (tempBusstopStringcount == 0)
                {
                    Errors.Add(new Error(
                        Filename,
                        reader.CurrentLine,
                        Text.IMPORT_BusstopStringcountNotFound,
                        false));
                }

                var currentBusstop = new Stop(reader.ReadLine());

                string line;
                for (int i = 0;
                    // continue if iterater is lower than stringcount OR  no stringcount was found
                    (tempBusstopStringcount == 0 || i < tempBusstopStringcount) &&
                    // AND ONLY if there is no separator
                    !(line = reader.ReadLine()).Contains(Keywords.OMSI1_SEPARATOR);
                    i++)
                {
                    currentBusstop.StringList.SetString(i, line);
                }
                NewHoffile.Stops.Add(currentBusstop);
            }
            catch (Exception e)
            {
                Errors.Add(new Error(
                    Filename,
                    reader.CurrentLine,
                    string.Format(Text.IMPORT_BusstopException, e.Message, e.TargetSite),
                    true));
            }

        }

        // [HOF_SUITE_DIRECTORIES]
        void AddExportDirectories(Reader reader)
        {
            try
            {
                string line;
                while ((line = reader.ReadLine()) != Keywords.SUITE_DIRECTORIES_END)
                {
                    NewHoffile.Settings.VehicleDirectories.Add(line);
                }
            }
            catch (Exception)
            {
                Errors.Add(new Error(
                    Filename,
                    reader.CurrentLine,
                    Text.IMPORT_SuiteDirectoriesException,
                    false));
            }

        }

        /// <summary>
        /// Link servicetrip destination to terminus list.
        /// </summary>
        void LinkServicetripTerminus(Reader reader)
        {
            var servicetripDestination = NewHoffile.Destinations.Find(x => x.Name == tempServicetrip);
            if (servicetripDestination == null)
            {
                Errors.Add(new Error(
                    Filename,
                    reader.CurrentLine,
                    string.Format(Text.IMPORT_ServicetripNotFound, tempServicetrip),
                    false));
            }
            else
            {
                NewHoffile.Settings.Servicetrip = servicetripDestination;
            }
        }
    }
}
