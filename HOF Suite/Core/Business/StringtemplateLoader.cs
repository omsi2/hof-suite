﻿using Hofsuite.Core.Structs;

using System;
using System.Collections.Generic;

namespace Hofsuite.Core.Business
{
    static class StringtemplateLoader
    {

        #region Private members and methods

        static SortedList<int, IStringtemplate> CurrentDestinationStringList { get; set; }
        static SortedList<int, IStringtemplate> CurrentStopStringList { get; set; }

        static readonly SortedList<int, IStringtemplate> defaultDestinationStringList = new SortedList<int, IStringtemplate>
        {
            {0, new Stringtemplate("IBIS 1", "IBIS 1", ">&&&&&&&&&&&&&&&&") },
            {1, new Stringtemplate("ANNAX (1)", "ANNAX (1)", "&&&&&&&&&&&&&&&&") },
            {2, new Stringtemplate("ANNAX (2)", "ANNAX (2)", "&&&&&&&&&&&&&&&&") },
            {3, new Stringtemplate("ANNAX (Seite)", "ANNAX (side)", "&&&&&&&&&&&&&&&&") },
            {4, new StringtemplateFile("Rollband-Textur", "RLB texture", ImageType.Rollsign) },
            {5, new Stringtemplate("IBIS 2", "IBIS 2", "&&&&&&&&&&&&&&&&&&&&") },
            {6, new StringtemplateFile("Steckschild-Textur|Steckschild-Textur für 1000er Ziele", "Add Plate texture|Add Plate file for 1000-destinations", ImageType.Addplate) },
            {7, new StringtemplateFile("Krueger-Textur|Krueger-Textur, die die Matrix ersetzt", "Krueger texture|Krueger texture which replaces the matrix display", ImageType.Krueger) },
            {8, new Stringtemplate("ANNAX Front|Für O307, O407 und MAN UEL", "ANNAX front|For O307, O407 and MAN UEL", "&&&&&&&&&&&&&&&") },
            {9, new Stringtemplate("ANNAX Heck|Für O307, O407 und MAN UEL", "ANNAX rear|For O307, O407 and MAN UEL", "&&&&") },
            {10, new Stringtemplate("Ikarus 280.02 Gegenroute", "Ikarus 280.02 reverse route") },
            {11, new Stringtemplate("Vollmatrix (1) (Busfanat)", "Full Display (1) (Busfanat)") },
            {12, new Stringtemplate("Vollmatrix (2) (Busfanat)", "Full Display (2) (Busfanat)") },
            {13, new Stringtemplate("LAWO (Front 1) (Cooper)", "LAWO (front 1) (Cooper)") },
            {14, new Stringtemplate("LAWO (Front 2) (Cooper)", "LAWO (front 2) (Cooper)") },
            {15, new Stringtemplate("LAWO (Seite 1) (Cooper)", "LAWO (side 1) (Cooper)") },
            {16, new Stringtemplate("LAWO (Seite 2) (Cooper)", "LAWO (side 2) (Cooper)") },
            {17, new Stringtemplate("IBIS (Wien)|IBIS-Anzeige für Niederflur-Busse aus den Wien-Add-Ons 1 und 2", "IBIS (Vienna)|IBIS display for Addon 1 and 2 buses") },
            {18, new Stringtemplate("MAN ÜL (1)", "MAN ÜL (1)") },
            {19, new Stringtemplate("MAN ÜL (2)", "MAN ÜL (2)") },
            {20, new Stringtemplate("MAN Stadtbus (Innenanzeige 1)", "MAN Stadtbus (inside display 1)") },
            {21, new Stringtemplate("MAN Stadtbus (Innenanzeige 2)|(zusätzliche Information z.B. 'über')", "MAN Stadtbus (inside display 1)|extra information e.g. 'via'") },
            {22, new Stringtemplate("LAWO (Heck) (Cooper)", "LAWO (rear) (Cooper)", "&&&&&&") },
            {23, new Stringtemplate("einfache Krüger-Matrix (1)", "simple Krueger matrix (1)") },
            {24, new Stringtemplate("einfache Krüger-Matrix (2)", "simple Krueger matrix (2)") },
            {25, new Stringtemplate("Anzeigecode (Wien)|Anzeigecode für das Ansagegerät im LU 200 / GU 240 aus Wien", "display code (Vienna)|display code for announcements (LU 200 / GU 240 from Vienna)") },
        };

        static readonly SortedList<int, IStringtemplate> defaultStopStringList = new SortedList<int, IStringtemplate>
        {
            {0, new Stringtemplate("IBIS 1", "IBIS 1", ">&&&&&&&&&&&&&&&&") },
            {1, new Stringtemplate("Innenanzeige 1", "Inside display 1") },
            {2, new Stringtemplate("Innenanzeige 2", "Inside display 2") },
            {3, new Stringtemplate("IBIS 2", "IBIS 2", "&&&&&&&&&&&&&&&&&&&&") },
            {4, new Stringtemplate("Haltestellen-Code (Wien)|Haltestellen- Code für Ansagengeräte", "Busstop code (Vienna)|Busstop code for announcements") },
            {5, new Stringtemplate("Wabencode (iTram)", "Busstop code (iTram)") },
            {6, new Stringtemplate("IBIS-Display (Wien)", "IBIS display (Vienna)") },
            {7, new Stringtemplate("Ansagenlänge (Wien)", "Announcement length (Vienna)") },
            {8, new Stringtemplate("Innenanzeige (Wien)|Innenanzeige, zwei Zeilen (getrennt mit '@') je 24 Zeichen", "Inside Display (Vienna)|Inside display, two lines (separated with '@') a 24 characters") },
        };

        /// <summary>
        /// Load custom strings from a filename. If the operation fails, reset all custom fields to default.
        /// </summary>
        static void LoadCustomStrings(string filename)
        {
            if (!PathBusiness.CheckIfFileExists(filename))
            {
                CurrentDestinationStringList = CurrentStopStringList = null;
                CurrentFileIsValid = false;
                return;
            }

            CurrentDestinationStringList = new SortedList<int, IStringtemplate>();
            CurrentStopStringList = new SortedList<int, IStringtemplate>();

            using (var reader = new Reader(filename))
            {
                try
                {
                    string line;
                    int countDestinationStrings = 0, countStopStrings = 0;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line == Keywords.CUSTOMSTRINGS_TERMINUS)
                        {
                            if (countDestinationStrings == 4)
                            {
                                CurrentDestinationStringList.Add(countDestinationStrings++, new StringtemplateFile(reader.ReadLine(), reader.ReadLine(), ImageType.Rollsign));
                            }
                            else if (countDestinationStrings == 6)
                            {
                                CurrentDestinationStringList.Add(countDestinationStrings++, new StringtemplateFile(reader.ReadLine(), reader.ReadLine(), ImageType.Addplate));
                            }
                            else if (countDestinationStrings == 7)
                            {
                                CurrentDestinationStringList.Add(countDestinationStrings++, new StringtemplateFile(reader.ReadLine(), reader.ReadLine(), ImageType.Krueger));
                            }
                            else
                            {
                                CurrentDestinationStringList.Add(countDestinationStrings++, new Stringtemplate(reader.ReadLine(), reader.ReadLine(), reader.ReadLine()));
                            }
                        }

                        else if (line == Keywords.CUSTOMSTRINGS_BUSSTOP)
                        {
                            CurrentStopStringList.Add(countStopStrings++, new Stringtemplate(reader.ReadLine(), reader.ReadLine(), reader.ReadLine()));
                        }
                    }

                    if (CurrentDestinationStringList.Count > 0 && CurrentStopStringList.Count > 0)
                    {
                        CurrentFileIsValid = true;
                    }
                    else
                    {
                        CurrentDestinationStringList = CurrentStopStringList = null;
                        CurrentFileIsValid = false;
                    }
                }
                catch (Exception)
                {
                    CurrentDestinationStringList = CurrentStopStringList = null;
                    CurrentFileIsValid = false;
                }
            }
        }

        static bool CurrentFileIsValid { get; set; } = false;

        #endregion

        /// <summary>
        /// Valid filename or null
        /// </summary>
        public static string ValidFilename
        {
            get => CurrentFileIsValid ? SettingsBusiness.Stringtemplate : "";
            set
            {
                LoadCustomStrings(value);
                SettingsBusiness.Stringtemplate = CurrentFileIsValid ? value : "";
            }
        }
        
        public static SortedList<int, IStringtemplate> GetDestinationStrings() =>
            CurrentFileIsValid ? CurrentDestinationStringList : defaultDestinationStringList;

        public static SortedList<int, IStringtemplate> GetStopStrings() =>
            CurrentFileIsValid ? CurrentStopStringList : defaultStopStringList;

        public static int GetDestinationStringcount() =>
            GetDestinationStrings().Count;

        public static int GetStopStringcount() =>
            GetStopStrings().Count;
    }
}
