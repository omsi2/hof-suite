﻿using Hofsuite.Core.Structs;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Hofsuite.Core.Business
{
    static class MapLoader
    {
        public static List<Map> GetAllMaps()
        {
            var result = new List<Map>();

            // Search in all subdirectories where global.cfg and TTData folder exists
            foreach (var directory in Directory.GetDirectories(PathBusiness.GetMapDirectory())
                .Where(x => File.Exists(x + "\\global.cfg") && Directory.Exists(x + "\\TTData")))
            {
                try
                {
                    string name = "";

                    // Read global.cfg
                    using (var reader = new StreamReader(directory + "\\global.cfg", Encoding.Default))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            if (line == Keywords.NAME)
                            {
                                // Set name
                                name = reader.ReadLine();
                                break;
                            }
                        }
                        if (name == "") throw new Exception($"Keyword {Keywords.NAME} was not found in global.cfg!");
                    }

                    // Add map with picture
                    if (File.Exists(directory + "\\picture.jpg"))
                    {
                        result.Add(new Map(directory, name, Image.FromFile(directory + "\\picture.jpg")));
                    }
                    // Add map without picture
                    else
                    {
                        result.Add(new Map(directory, name, null));
                    }
                }
                catch (Exception)
                {
                    // error: do nothing, this map could not be loaded
                }
            }

            return result;
        }

        static Dictionary<int, string> GetBusstopIdDictionary(Map map)
        {
            var result = new Dictionary<int, string>();

            // Read file
            string filename = map.Path + "\\TTdata\\Busstops.cfg";

            using (var reader = new StreamReader(filename, Encoding.Default))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    try
                    {
                        if (line == Keywords.TRIP_BUSSTOP)
                        {
                            // if id could be detected, add an entry to the dictionary
                            string name = reader.ReadLine();
                            reader.ReadLine();
                            if (int.TryParse(reader.ReadLine(), out int id)) result.Add(id, name);
                        }
                    }
                    catch (Exception)
                    {
                        // do nothing, not add busstop to dictionary
                    }
                }
            }

            return result;
        }

        public static List<TripFile> GetAllTripFiles(Map map)
        {
            var trips = new List<TripFile>();

            // Search in all trip files in the directory
            foreach (var filename in Directory.GetFiles(map.Path + "\\TTData", "*.TTP", SearchOption.TopDirectoryOnly))
            {
                var trip = new TripFile(Path.GetFileNameWithoutExtension(filename));
                Dictionary<int, string> busstopDictionary = null;

                using (var reader = new StreamReader(filename, Encoding.Default))
                {
                    try
                    {
                        bool propertiesDetected = false;
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            switch (line)
                            {
                                // Trip properties
                                case Keywords.TRIP:
                                    reader.ReadLine();
                                    trip.Destination = reader.ReadLine();
                                    trip.Line = reader.ReadLine();
                                    propertiesDetected = true;
                                    break;

                                // Station (Type1)
                                case Keywords.TRIP_STATION:
                                    trip.IsStationLink = false;
                                    reader.ReadLine();
                                    reader.ReadLine();
                                    trip.Stops.Add(reader.ReadLine());
                                    break;

                                // Station (Type2)
                                case Keywords.TRIP_STATION_TYPE2:
                                    trip.IsStationLink = true;
                                    if (int.TryParse(reader.ReadLine(), out int i))
                                        trip.Stops.Add(i.ToString());
                                    break;
                            }
                        }

                        // Link Type2 trips if necessary
                        if (trip.IsStationLink)
                        {
                            if (busstopDictionary == null) busstopDictionary = GetBusstopIdDictionary(map);
                            trip.Stops = trip.Stops.Select(x => busstopDictionary[int.Parse(x)]).ToList();
                        }

                        // only add valid trips
                        if (propertiesDetected) trips.Add(trip);
                    }
                    catch (Exception)
                    {
                        // do nothing, don't add trip to list
                    }
                }
            }
            trips.Sort();
            return trips;
        }

        public static Hof GetHoffileFromTrips(Map selectedMap, List<TripFile> trips)
        {
            var result = new Hof();

            // Change display name to map name
            var mapName = selectedMap.Displayname;
            result.Settings.Name = mapName;
            result.Settings.SetGlobalString(GlobalStringType.AnnouncementFolder, mapName);
            result.Settings.SetGlobalString(GlobalStringType.RollsignFolder, mapName);
            result.Settings.SetGlobalString(GlobalStringType.SideplateFolder, mapName);
            result.Settings.SetGlobalString(GlobalStringType.IBISCode5, "00");
            result.Settings.SetGlobalString(GlobalStringType.IBISCode8, "00");
            result.Settings.SetGlobalString(GlobalStringType.IBISCode9, "00");

            // Add default "Empty" terminus
            var serviceTripDestination = new Destination("Empty", 0, true);
            result.Destinations.Add(serviceTripDestination);
            result.Settings.Servicetrip = serviceTripDestination;
            int countTermini = 1;

            foreach (var trip in trips)
            {
                // Create a new route
                var route = new Route(trip.Filename, GetIntegerLineNumber(trip.Line))
                {
                    Name = trip.Filename,
                    Line = GetIntegerLineNumber(trip.Line)
                };
                int findNextTourNo = Route.FindNextTourNumber(route.Line, result.Routes);
                route.Tour = findNextTourNo > 99 ? 99 : findNextTourNo;

                // Find destination Terminus and create if necessary
                var destination = result.Destinations.Find(x => x.Name == trip.Destination);
                if (destination == null)
                {
                    destination = new Destination(trip.Destination, countTermini++);
                    result.Destinations.Add(destination);
                }
                route.Destination = destination;

                foreach (var stopString in trip.Stops)
                {
                    // Find stop and create if necessary
                    var stop = result.Stops.Find(x => x.Name == stopString);
                    if (stop == null)
                    {
                        stop = new Stop(stopString);
                        result.Stops.Add(stop);
                    }
                    route.Stops.Add(stop);
                }

                // Add route to list
                result.Routes.Add(route);
            }

            return result;
        }

        static int GetIntegerLineNumber(string line)
        {
            // Try direct parsing
            if (line.Length < 4 && int.TryParse(line.Trim(), out int result)) return result;

            // Try remove all letters
            char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzÄÖÜäöü.-,".ToCharArray();
            if (line.Trim(alphabet).Length < 4 && int.TryParse(line.Trim(alphabet), out result)) return result;

            // Return default value
            return 1;
        }
    }
}
