﻿namespace Hofsuite.GUI.Controls
{
    partial class HofSettingsView
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HofSettingsView));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.general_displayname = new System.Windows.Forms.TextBox();
            this.general_servicetrip = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.folder_sideplates_create = new System.Windows.Forms.Button();
            this.folder_rollsigns_create = new System.Windows.Forms.Button();
            this.folder_rollsigns_exists = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.folder_announcements = new System.Windows.Forms.TextBox();
            this.folder_announcements_exists = new System.Windows.Forms.PictureBox();
            this.folder_announcements_create = new System.Windows.Forms.Button();
            this.folder_sideplates_exists = new System.Windows.Forms.PictureBox();
            this.folder_rollsigns = new System.Windows.Forms.TextBox();
            this.folder_sideplates = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ibiscode_9xx = new System.Windows.Forms.MaskedTextBox();
            this.ibiscode_8xx = new System.Windows.Forms.MaskedTextBox();
            this.ibiscode_5xx = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.exportDirs = new System.Windows.Forms.CheckedListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.folder_rollsigns_exists)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.folder_announcements_exists)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.folder_sideplates_exists)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.tableLayoutPanel3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            this.toolTip.SetToolTip(this.groupBox1, resources.GetString("groupBox1.ToolTip"));
            // 
            // tableLayoutPanel3
            // 
            resources.ApplyResources(this.tableLayoutPanel3, "tableLayoutPanel3");
            this.tableLayoutPanel3.Controls.Add(this.general_displayname, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.general_servicetrip, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.toolTip.SetToolTip(this.tableLayoutPanel3, resources.GetString("tableLayoutPanel3.ToolTip"));
            // 
            // general_displayname
            // 
            resources.ApplyResources(this.general_displayname, "general_displayname");
            this.general_displayname.Name = "general_displayname";
            this.toolTip.SetToolTip(this.general_displayname, resources.GetString("general_displayname.ToolTip"));
            this.general_displayname.TextChanged += new System.EventHandler(this.DisplaynameChanged);
            // 
            // general_servicetrip
            // 
            resources.ApplyResources(this.general_servicetrip, "general_servicetrip");
            this.general_servicetrip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.general_servicetrip.DropDownWidth = 300;
            this.general_servicetrip.FormattingEnabled = true;
            this.general_servicetrip.Name = "general_servicetrip";
            this.toolTip.SetToolTip(this.general_servicetrip, resources.GetString("general_servicetrip.ToolTip"));
            this.general_servicetrip.SelectedIndexChanged += new System.EventHandler(this.ServicetripChanged);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            this.toolTip.SetToolTip(this.label1, resources.GetString("label1.ToolTip"));
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            this.toolTip.SetToolTip(this.label2, resources.GetString("label2.ToolTip"));
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Controls.Add(this.tableLayoutPanel4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            this.toolTip.SetToolTip(this.groupBox2, resources.GetString("groupBox2.ToolTip"));
            // 
            // tableLayoutPanel4
            // 
            resources.ApplyResources(this.tableLayoutPanel4, "tableLayoutPanel4");
            this.tableLayoutPanel4.Controls.Add(this.folder_sideplates_create, 3, 2);
            this.tableLayoutPanel4.Controls.Add(this.folder_rollsigns_create, 3, 1);
            this.tableLayoutPanel4.Controls.Add(this.folder_rollsigns_exists, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.folder_announcements, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.folder_announcements_exists, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.folder_announcements_create, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.folder_sideplates_exists, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.folder_rollsigns, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.folder_sideplates, 1, 2);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.toolTip.SetToolTip(this.tableLayoutPanel4, resources.GetString("tableLayoutPanel4.ToolTip"));
            // 
            // folder_sideplates_create
            // 
            resources.ApplyResources(this.folder_sideplates_create, "folder_sideplates_create");
            this.folder_sideplates_create.Name = "folder_sideplates_create";
            this.toolTip.SetToolTip(this.folder_sideplates_create, resources.GetString("folder_sideplates_create.ToolTip"));
            this.folder_sideplates_create.UseVisualStyleBackColor = true;
            this.folder_sideplates_create.Click += new System.EventHandler(this.SideplateClick);
            // 
            // folder_rollsigns_create
            // 
            resources.ApplyResources(this.folder_rollsigns_create, "folder_rollsigns_create");
            this.folder_rollsigns_create.Name = "folder_rollsigns_create";
            this.toolTip.SetToolTip(this.folder_rollsigns_create, resources.GetString("folder_rollsigns_create.ToolTip"));
            this.folder_rollsigns_create.UseVisualStyleBackColor = true;
            this.folder_rollsigns_create.Click += new System.EventHandler(this.RollsignClick);
            // 
            // folder_rollsigns_exists
            // 
            resources.ApplyResources(this.folder_rollsigns_exists, "folder_rollsigns_exists");
            this.folder_rollsigns_exists.Name = "folder_rollsigns_exists";
            this.folder_rollsigns_exists.TabStop = false;
            this.toolTip.SetToolTip(this.folder_rollsigns_exists, resources.GetString("folder_rollsigns_exists.ToolTip"));
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            this.toolTip.SetToolTip(this.label5, resources.GetString("label5.ToolTip"));
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            this.toolTip.SetToolTip(this.label3, resources.GetString("label3.ToolTip"));
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            this.toolTip.SetToolTip(this.label4, resources.GetString("label4.ToolTip"));
            // 
            // folder_announcements
            // 
            resources.ApplyResources(this.folder_announcements, "folder_announcements");
            this.folder_announcements.Name = "folder_announcements";
            this.toolTip.SetToolTip(this.folder_announcements, resources.GetString("folder_announcements.ToolTip"));
            this.folder_announcements.TextChanged += new System.EventHandler(this.AnnouncementsChanged);
            // 
            // folder_announcements_exists
            // 
            resources.ApplyResources(this.folder_announcements_exists, "folder_announcements_exists");
            this.folder_announcements_exists.Name = "folder_announcements_exists";
            this.folder_announcements_exists.TabStop = false;
            this.toolTip.SetToolTip(this.folder_announcements_exists, resources.GetString("folder_announcements_exists.ToolTip"));
            // 
            // folder_announcements_create
            // 
            resources.ApplyResources(this.folder_announcements_create, "folder_announcements_create");
            this.folder_announcements_create.Name = "folder_announcements_create";
            this.toolTip.SetToolTip(this.folder_announcements_create, resources.GetString("folder_announcements_create.ToolTip"));
            this.folder_announcements_create.UseVisualStyleBackColor = true;
            this.folder_announcements_create.Click += new System.EventHandler(this.AnnouncementsClick);
            // 
            // folder_sideplates_exists
            // 
            resources.ApplyResources(this.folder_sideplates_exists, "folder_sideplates_exists");
            this.folder_sideplates_exists.Name = "folder_sideplates_exists";
            this.folder_sideplates_exists.TabStop = false;
            this.toolTip.SetToolTip(this.folder_sideplates_exists, resources.GetString("folder_sideplates_exists.ToolTip"));
            // 
            // folder_rollsigns
            // 
            resources.ApplyResources(this.folder_rollsigns, "folder_rollsigns");
            this.folder_rollsigns.Name = "folder_rollsigns";
            this.toolTip.SetToolTip(this.folder_rollsigns, resources.GetString("folder_rollsigns.ToolTip"));
            this.folder_rollsigns.TextChanged += new System.EventHandler(this.RollsignChanged);
            // 
            // folder_sideplates
            // 
            resources.ApplyResources(this.folder_sideplates, "folder_sideplates");
            this.folder_sideplates.Name = "folder_sideplates";
            this.toolTip.SetToolTip(this.folder_sideplates, resources.GetString("folder_sideplates.ToolTip"));
            this.folder_sideplates.TextChanged += new System.EventHandler(this.SideplateChanged);
            // 
            // groupBox3
            // 
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Controls.Add(this.tableLayoutPanel2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            this.toolTip.SetToolTip(this.groupBox3, resources.GetString("groupBox3.ToolTip"));
            // 
            // tableLayoutPanel2
            // 
            resources.ApplyResources(this.tableLayoutPanel2, "tableLayoutPanel2");
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.ibiscode_9xx, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.ibiscode_8xx, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.ibiscode_5xx, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label9, 2, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.toolTip.SetToolTip(this.tableLayoutPanel2, resources.GetString("tableLayoutPanel2.ToolTip"));
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            this.toolTip.SetToolTip(this.label8, resources.GetString("label8.ToolTip"));
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            this.toolTip.SetToolTip(this.label6, resources.GetString("label6.ToolTip"));
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            this.toolTip.SetToolTip(this.label7, resources.GetString("label7.ToolTip"));
            // 
            // ibiscode_9xx
            // 
            resources.ApplyResources(this.ibiscode_9xx, "ibiscode_9xx");
            this.ibiscode_9xx.Name = "ibiscode_9xx";
            this.toolTip.SetToolTip(this.ibiscode_9xx, resources.GetString("ibiscode_9xx.ToolTip"));
            this.ibiscode_9xx.TextChanged += new System.EventHandler(this.Ibiscode9Changed);
            // 
            // ibiscode_8xx
            // 
            resources.ApplyResources(this.ibiscode_8xx, "ibiscode_8xx");
            this.ibiscode_8xx.Name = "ibiscode_8xx";
            this.toolTip.SetToolTip(this.ibiscode_8xx, resources.GetString("ibiscode_8xx.ToolTip"));
            this.ibiscode_8xx.TextChanged += new System.EventHandler(this.Ibiscode8Changed);
            // 
            // ibiscode_5xx
            // 
            resources.ApplyResources(this.ibiscode_5xx, "ibiscode_5xx");
            this.ibiscode_5xx.Name = "ibiscode_5xx";
            this.toolTip.SetToolTip(this.ibiscode_5xx, resources.GetString("ibiscode_5xx.ToolTip"));
            this.ibiscode_5xx.TextChanged += new System.EventHandler(this.Ibiscode5Changed);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            this.tableLayoutPanel2.SetRowSpan(this.label9, 3);
            this.toolTip.SetToolTip(this.label9, resources.GetString("label9.ToolTip"));
            // 
            // exportDirs
            // 
            resources.ApplyResources(this.exportDirs, "exportDirs");
            this.exportDirs.CheckOnClick = true;
            this.exportDirs.FormattingEnabled = true;
            this.exportDirs.Name = "exportDirs";
            this.toolTip.SetToolTip(this.exportDirs, resources.GetString("exportDirs.ToolTip"));
            this.exportDirs.SelectedIndexChanged += new System.EventHandler(this.ExportSelectionChanged);
            // 
            // groupBox4
            // 
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Controls.Add(this.exportDirs);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            this.toolTip.SetToolTip(this.groupBox4, resources.GetString("groupBox4.ToolTip"));
            // 
            // MainSettingsView
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "MainSettingsView";
            this.toolTip.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.folder_rollsigns_exists)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.folder_announcements_exists)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.folder_sideplates_exists)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox general_servicetrip;
        private System.Windows.Forms.TextBox general_displayname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button folder_sideplates_create;
        private System.Windows.Forms.Button folder_rollsigns_create;
        private System.Windows.Forms.PictureBox folder_rollsigns_exists;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox folder_announcements;
        private System.Windows.Forms.PictureBox folder_announcements_exists;
        private System.Windows.Forms.Button folder_announcements_create;
        private System.Windows.Forms.PictureBox folder_sideplates_exists;
        private System.Windows.Forms.TextBox folder_rollsigns;
        private System.Windows.Forms.TextBox folder_sideplates;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox ibiscode_9xx;
        private System.Windows.Forms.MaskedTextBox ibiscode_8xx;
        private System.Windows.Forms.MaskedTextBox ibiscode_5xx;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckedListBox exportDirs;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}
