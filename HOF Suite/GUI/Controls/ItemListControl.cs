﻿using Hofsuite.Core;
using Hofsuite.Core.Structs;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Hofsuite.GUI.Controls
{
    partial class ItemListControl : UserControl
    {
        public ItemListControl(ItemType type)
        {
            InitializeComponent();

            ItemType = type;
            switch (ItemType)
            {
                case ItemType.Destination:
                    addAddplate.Visible = true;
                    addAddplate.Enabled = true;
                    addKpp.Visible = true;
                    addKpp.Enabled = true;
                    break;

                case ItemType.Stop:
                    sortItemsByCode.Visible = false;
                    sortItemsByCode.Enabled = false;
                    break;

                case ItemType.Route:
                    break;

                default:
                    throw new ArgumentException("Enum not valid! Type can only be Destination, Stop or Route!");
            }

            ApplicationState.HofChanged += LoadApplicationState;
            ApplicationState.ItemChanged += ItemChanged;
            ApplicationState.ItemPropertiesChanged += ItemPropertiesChanged;
            ApplicationState.TabChanged += TabChanged;

            LoadApplicationState(this, EventArgs.Empty);
        }

        private void TabChanged(object sender, EventArgs e)
        {
            ApplicationState.HofChanged -= LoadApplicationState;
            ApplicationState.ItemChanged -= ItemChanged;
            ApplicationState.ItemPropertiesChanged -= ItemPropertiesChanged;
            ApplicationState.TabChanged -= TabChanged;
        }

        private void LoadApplicationState(object sender, EventArgs e)
        {
            switch (ItemType)
            {
                case ItemType.Destination:
                    List = Hof.Destinations;
                    break;
                case ItemType.Stop:
                    List = Hof.Stops;
                    break;
                case ItemType.Route:
                    List = Hof.Routes;
                    break;
            }
            SynchronizeList();
            try
            {
                listBox.SelectedIndex = 0;
            }
            catch (Exception)
            {
                ItemChanged(sender, e);
            }
        }

        private void ItemChanged(object sender, EventArgs e)
        {
            int count = listBox.Items.Count;
            if (count == 0)
            {
                itemLabel.Text = Properties.Text.NAV_NoItems;
            }
            else if (listBox.SelectedItem == null)
            {
                itemLabel.Text = string.Format(Properties.Text.NAV_Items, count);
            }
            else
            {
                itemLabel.Text = $"{listBox.SelectedIndex + 1} / {count}";
            }
        }

        Hof Hof => ApplicationState.Hof;

        void SetUnsaved() => ApplicationState.IsUnsaved = true;

        IList List { get; set; }

        private void AddItem(object sender, EventArgs e)
        {
            Item newItem = null;
            switch (ItemType)
            {
                case ItemType.Destination:
                    newItem = new Destination();
                    break;
                case ItemType.Stop:
                    newItem = new Stop();
                    break;
                case ItemType.Route:
                    newItem = new Route();
                    break;
            }

            List.Add(newItem);
            listBox.Items.Add(newItem);
            SelectItem(listBox.Items.Count - 1);
            SetUnsaved();
        }

        private void DuplicateItem(object sender, EventArgs e)
        {
            if (listBox.SelectedItem is Item selectedItem)
            {
                var newItem = selectedItem.DuplicateItem();
                int index = listBox.SelectedIndex + 1;

                List.Insert(index, newItem);
                listBox.Items.Insert(index, newItem);
                SelectItem(index);
                SetUnsaved();
            }
            else
            {
                Program.Beep();
            }
        }

        private void RemoveItem(object sender, EventArgs e)
        {
            int index = listBox.SelectedIndex;
            if (index != -1)
            {
                List.RemoveAt(index);
                listBox.Items.RemoveAt(index);
                SelectItem(index);
                SetUnsaved();
            }
            else
            {
                Program.Beep();
            }
        }

        private void MoveItemUp(object sender, EventArgs e)
        {
            int index = listBox.SelectedIndex;
            try
            {
                List.MoveItem(index, -1);
                listBox.Items.MoveItem(index, -1);
                SelectItem(index - 1);
                SetUnsaved();
            }
            catch (ArgumentOutOfRangeException)
            {
                Program.Beep();
            }
        }

        private void MoveItemDown(object sender, EventArgs e)
        {
            int index = listBox.SelectedIndex;
            try
            {
                List.MoveItem(index, 1);
                listBox.Items.MoveItem(index, 1);
                SelectItem(index + 1);
                SetUnsaved();
            }
            catch (ArgumentOutOfRangeException)
            {
                Program.Beep();
            }
        }

        private void SortByCode(object sender, EventArgs e)
        {
            switch (ItemType)
            {
                case ItemType.Destination:
                    (List as List<Destination>).Sort((x, y) => x.CompareTo(y));
                    break;
                case ItemType.Route:
                    (List as List<Route>).Sort((x, y) => x.CompareTo(y));
                    break;
            }
            SynchronizeList();
            SetUnsaved();
            ApplicationState.Item = null;
        }

        private void SortByName(object sender, EventArgs e)
        {
            switch (ItemType)
            {
                case ItemType.Destination:
                    (List as List<Destination>).Sort((x, y) => x.CompareByName(y));
                    break;
                case ItemType.Stop:
                    (List as List<Stop>).Sort((x, y) => x.CompareTo(y));
                    break;
                case ItemType.Route:
                    (List as List<Route>).Sort((x, y) => x.CompareByName(y));
                    break;
            }
            SynchronizeList();
            SetUnsaved();
            ApplicationState.Item = null;
        }

        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SkipEvents == true) return;
            switch (ItemType)
            {
                case ItemType.Destination:
                    ApplicationState.Item = listBox.SelectedItem as Destination;
                    break;
                case ItemType.Stop:
                    ApplicationState.Item = listBox.SelectedItem as Stop;
                    break;
                case ItemType.Route:
                    ApplicationState.Item = listBox.SelectedItem as Route;
                    break;
            }
            
        }

        private void NextItem(object sender, EventArgs e)
        {
            try
            {
                listBox.SelectedIndex++;
            }
            catch (ArgumentOutOfRangeException)
            {
                Program.Beep();
                ApplicationState.Item = null;
            }
        }

        private void PreviousItem(object sender, EventArgs e)
        {
            try
            {
                listBox.SelectedIndex--;
            }
            catch (ArgumentOutOfRangeException)
            {
                Program.Beep();
                ApplicationState.Item = null;
            }
        }

        void SynchronizeList()
        {
            listBox.Items.Clear();
            foreach (var item in List) listBox.Items.Add(item);
        }

        ItemType ItemType { get; }

        void SelectItem(int index)
        {
            try
            {
                listBox.SelectedIndex = index;
            }
            catch (ArgumentOutOfRangeException)
            {
                try
                {
                    listBox.SelectedIndex = index - 1;
                }
                catch (ArgumentOutOfRangeException)
                {
                    try
                    {
                        listBox.SelectedIndex = 0;
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        Program.Beep();
                        ApplicationState.Item = null;
                    }
                }
            }
        }

        private void AddAddplateDestination(object sender, EventArgs e)
        {
            Item newItem = new Destination(code: 1000);

            List.Add(newItem);
            listBox.Items.Add(newItem);
            SelectItem(listBox.Items.Count - 1);
            SetUnsaved();
        }

        private void AddKppDestinations(object sender, EventArgs e)
        {
            if (MessageBox.Show(Properties.Text.MSG_CreateKppDestinations + " " + Properties.Text.ASK_Continue,
                Properties.Text.TITLE_Information, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                if (List.OfType<Destination>().FirstOrDefault(x => x.Code == 14990) == null)
                {
                    Destination newItem = new Destination("Options", 14990);
                    List.Add(newItem);
                    listBox.Items.Add(newItem);
                }
                
                foreach (var item in new List<Destination>(List.OfType<Destination>().Where(x => x.Code < 1000)))
                {
                    if (List.OfType<Destination>().FirstOrDefault(x => x.Code == item.Code + 20000) == null)
                    {
                        Destination newItem = new Destination(item.Name, item.Code + 20000, item.Allexit, new StringList(item.StringList));
                        List.Add(newItem);
                        listBox.Items.Add(newItem);
                    }
                }
                SetUnsaved();
            }
        }

        bool SkipEvents { get; set; }

        private void ItemPropertiesChanged(object sender, EventArgs e)
        {
            if (listBox.SelectedItem == null) return;
            SkipEvents = true;
            listBox.Items[listBox.SelectedIndex] = ApplicationState.Item;
            SkipEvents = false;
        }
    }
}
