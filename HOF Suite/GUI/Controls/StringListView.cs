﻿using Hofsuite.Core;
using Hofsuite.Core.Business;
using Hofsuite.Core.Structs;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace Hofsuite.GUI.Controls
{
    partial class StringListView : UserControl
    {
        public StringListView(StringType type)
        {
            InitializeComponent();
            Type = type;
            Strings = type == StringType.Destination ? StringtemplateLoader.GetDestinationStrings() : StringtemplateLoader.GetStopStrings();

            InitializeTextboxes(CultureInfo.InstalledUICulture.TwoLetterISOLanguageName);
            InitializeButtons();
            LoadStrings(this, EventArgs.Empty);

            ApplicationState.MaskChanged += LoadMask;
            ApplicationState.ItemChanged += LoadStrings;
            ApplicationState.TabChanged += TabChanged;
        }

        private void TabChanged(object sender, EventArgs e)
        {
            ApplicationState.MaskChanged -= LoadMask;
            ApplicationState.ItemChanged -= LoadStrings;
            ApplicationState.TabChanged -= TabChanged;
        }

        void SetUnsaved() => ApplicationState.IsUnsaved = true;

        bool Initializing { get; set; }

        StringList List { get; set; }

        StringType Type { get; }

        SortedList<int, IStringtemplate> Strings { get; }

        void InitializeTextboxes(string language)
        {
            Initializing = true;
            Font font = new Font("Courier New", 9F);

            int currentRow = 0;
            tableLayoutPanel.RowStyles.Clear();
            foreach (var item in Strings)
            {
                tableLayoutPanel.Controls.Add(new Label
                {

                    Dock = DockStyle.Fill,
                    Text = item.Key.ToString(),
                    TextAlign = ContentAlignment.MiddleRight,

                }, 0, currentRow);

                var label = new Label
                {
                    Dock = DockStyle.Fill,
                    Text = language == "de" ? item.Value.Name_DE.Split('|')[0] : item.Value.Name_EN.Split('|')[0],
                    TextAlign = ContentAlignment.MiddleRight

                };

                if (item.Value.Name_DE.Split('|').Length == 2)
                    toolTip.SetToolTip(label, language == "de" ? item.Value.Name_DE.Split('|')[1] : item.Value.Name_EN.Split('|')[1]);
                tableLayoutPanel.Controls.Add(label, 1, currentRow);

                var textBox = new MaskedTextBox
                {
                    Dock = DockStyle.Fill,
                    TextAlign = HorizontalAlignment.Center,

                    PromptChar = '\u2022',
                    Anchor = (AnchorStyles.Left | AnchorStyles.Right),
                    TextMaskFormat = MaskFormat.IncludePromptAndLiterals,
                    Font = font,
                };
                textBox.TextChanged += SaveString;
                tableLayoutPanel.Controls.Add(textBox, 2, currentRow);

                tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 30));
                currentRow++;
            }

            Control placeholder = new Control();
            tableLayoutPanel.Controls.Add(placeholder, 0, currentRow);
            tableLayoutPanel.RowCount = currentRow;

            Initializing = false;
        }

        void InitializeButtons()
        {
            Initializing = true;

            if (Type == StringType.Stop)
            {
                var button = new Button()
                {
                    Image = Properties.Resources.Circle,
                    Dock = DockStyle.Fill,
                };
                toolTip.SetToolTip(button, Properties.Text.Busstop_Autocomplete);
                tableLayoutPanel.Controls.Add(button, 3, 0);
                tableLayoutPanel.SetRowSpan(button, 3);
                button.Click += BusstopAutocomplete;
            }

            Initializing = false;
        }

        private void BusstopAutocomplete(object sender, EventArgs e)
        {
            if (ApplicationState.Item as Stop != null)
            {
                ((Stop)ApplicationState.Item).Autocomplete();
                LoadStrings(sender, e);
            }
        }

        private void LoadStrings(object sender, EventArgs e)
        {
            if (ApplicationState.Item == null)
            {
                Visible = false;
                return;
            }
            else
            {
                Visible = true;
            }

            Initializing = true;
            List = ((IHasStringList)ApplicationState.Item).StringList;

            for (int i = 0; i < tableLayoutPanel.RowCount; i++)
            {
                MaskedTextBox textBox = (MaskedTextBox)tableLayoutPanel.GetControlFromPosition(2, i);
                textBox.Text = List.GetString(i);
            }

            Initializing = false;
            LoadMask(sender, e);
        }

        private void SaveString(object sender, EventArgs e)
        {
            if (Initializing) return;

            int row = tableLayoutPanel.GetPositionFromControl((Control)sender).Row;
            List.SetString(row, ((MaskedTextBox)sender).Text.Replace('\u2022', ' '));
            SetUnsaved();
        }

        private void LoadMask(object sender, EventArgs e)
        {
            Initializing = true;

            for (int i = 0; i < tableLayoutPanel.RowCount; i++)
            {
                MaskedTextBox textBox = (MaskedTextBox)tableLayoutPanel.GetControlFromPosition(2, i);
                textBox.Mask = SettingsBusiness.MaskEnabled ? Strings[i].Mask : "";
            }

            Initializing = false;
        }
    }
}
