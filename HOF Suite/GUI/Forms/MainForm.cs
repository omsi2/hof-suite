﻿using Hofsuite.Core;
using Hofsuite.Core.Business;
using Hofsuite.Core.Structs;
using Hofsuite.GUI.Controls;
using Hofsuite.Properties;

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;

namespace Hofsuite.GUI.Forms
{
    partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            ApplicationState.FilenameChanged += FilenameChanged;
            ApplicationState.TabChanged += TabChanged;
            ApplicationState.UnsavedChanged += UnsavedChanged;

            LoadHof(new Hof(), false);
            InitializeForm();
        }

        public MainForm(string filename)
        {
            InitializeComponent();

            ApplicationState.FilenameChanged += FilenameChanged;
            ApplicationState.TabChanged += TabChanged;
            ApplicationState.UnsavedChanged += UnsavedChanged;

            LoadFile(filename);
            InitializeForm();
        }

        void InitializeForm()
        {
            statusProgramLabel.Text = Program.GetApplicationName(true);
            ApplicationState.Tab = ShownTab.Settings;
        }

        private void DisposeControl(Control control)
        {
            foreach (Control c in control.Controls)
            {
                DisposeControl(c);
            }
            try
            {
                control.Dispose();
            }
            catch (Exception)
            {
            }
            
        }

        private void TabChanged(object sender, EventArgs e)
        {
            var controls = new List<Control>(tabContent.Controls.OfType<Control>());
            foreach (var control in controls)
            {
                DisposeControl(control);
                tabContent.Controls.Remove(control);
                control.Dispose();
            }

            ApplicationState.Item = null;

            Control newControl = null;
            switch (ApplicationState.Tab)
            {
                case ShownTab.Settings:
                    tabSettings.Image = Resources.Tab_Settings_act;
                    tabDestinations.Image = Resources.Tab_Destinations;
                    tabStops.Image = Resources.Tab_Stops;
                    tabRoutes.Image = Resources.Tab_Routes;
                    newControl = new HofSettingsView();
                    break;

                case ShownTab.Destinations:
                    tabSettings.Image = Resources.Tab_Settings;
                    tabDestinations.Image = Resources.Tab_Destinations_act;
                    tabStops.Image = Resources.Tab_Stops;
                    tabRoutes.Image = Resources.Tab_Routes;
                    newControl = new DestinationView();
                    break;

                case ShownTab.Stops:
                    tabSettings.Image = Resources.Tab_Settings;
                    tabDestinations.Image = Resources.Tab_Destinations;
                    tabStops.Image = Resources.Tab_Stops_act;
                    tabRoutes.Image = Resources.Tab_Routes;
                    newControl = new StopView();
                    break;

                case ShownTab.Routes:
                    tabSettings.Image = Resources.Tab_Settings;
                    tabDestinations.Image = Resources.Tab_Destinations;
                    tabStops.Image = Resources.Tab_Stops;
                    tabRoutes.Image = Resources.Tab_Routes_act;
                    newControl = new RouteView();
                    break;

                default:
                    throw new ArgumentException("Enum is not valid!");
            }

            newControl.Dock = DockStyle.Fill;
            tabContent.Controls.Add(newControl);
        }

        private void FilenameChanged(object sender, EventArgs e)
        {
            string filename = ApplicationState.Filename;
            if (filename == null)
            {
                statusFileLabel.Text = Properties.Text.FILE_New;
                statusFileLabel.Image = Resources.EmptyFile;
                statusFileLabel.IsLink = false;
                Text = $"{(ApplicationState.IsUnsaved ? "* " : "")}{Properties.Text.FILE_New} - {Program.GetApplicationName(false)}";
            }
            else
            {
                statusFileLabel.Text = PathBusiness.ShortenFilename(filename);
                statusFileLabel.Image = Resources.File;
                statusFileLabel.IsLink = true;
                Text = $"{(ApplicationState.IsUnsaved ? "* " : "")}{PathBusiness.GetFilenameFromPath(filename)} - {Program.GetApplicationName(false)}";
            }
        }

        private void UnsavedChanged(object sender, EventArgs e)
        {
            string filename = ApplicationState.Filename;
            if (filename == null)
            {
                Text = $"{(ApplicationState.IsUnsaved ? "* " : "")}{Properties.Text.FILE_New} - {Program.GetApplicationName(false)}";
            }
            else
            {
                Text = $"{(ApplicationState.IsUnsaved ? "* " : "")}{PathBusiness.GetFilenameFromPath(filename)} - {Program.GetApplicationName(false)}";
            }
        }

        void ClosingEvent(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !CloseFile();
        }

        private void NewClick(object sender, EventArgs e)
        {
            if (!CloseFile()) return;

            LoadHof(new Hof(), false);
        }

        private void ImportClick(object sender, EventArgs e)
        {
            if (!CloseFile()) return;

            var form = new ImportForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                LoadHof(form.NewHof, true);
            }
        }

        private void OpenClick(object sender, EventArgs e)
        {
            if (!CloseFile()) return;

            openFileDialog.InitialDirectory = PathBusiness.CheckIfDirectoryExists(PathBusiness.GetVehicleDirectory()) ?
                PathBusiness.GetVehicleDirectory() : Environment.SpecialFolder.Desktop.ToString();

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                LoadFile(openFileDialog.FileName);
            }
        }

        private void SaveClick(object sender, EventArgs e)
        {
            if (!PathBusiness.CheckIfFileExists(ApplicationState.Filename))
            {
                SaveAsClick(sender, e);
            }
            else
            {
                SaveFile(ApplicationState.Filename);
            }
        }

        private void SaveAsClick(object sender, EventArgs e)
        {
            saveFileDialog.InitialDirectory = PathBusiness.CheckIfDirectoryExists(PathBusiness.GetVehicleDirectory()) ?
                PathBusiness.GetVehicleDirectory() : Environment.SpecialFolder.Desktop.ToString();

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                SaveFile(saveFileDialog.FileName);
            }
        }

        private void ExportClick(object sender, EventArgs e)
        {
            ExportFile();
        }

        private void SettingsClick(object sender, EventArgs e)
        {
            new SettingsForm().ShowDialog();
            ApplicationState.Item = null;
            ApplicationState.Tab = ShownTab.Settings;
        }

        private void DownloadClick(object sender, EventArgs e)
        {
            Program.OpenLink(SettingsBusiness.GetLink(HyperlinkType.Webdisk));
        }

        private void SupportClick(object sender, EventArgs e)
        {
            Program.OpenLink(SettingsBusiness.GetLink(HyperlinkType.Forum));
        }

        private void ManualClick(object sender, EventArgs e)
        {
            Program.OpenLink(Properties.Text.FILE_Documentation);
        }

        private void AboutClick(object sender, EventArgs e)
        {
            new AboutForm().ShowDialog();
        }

        private void TabSettingsClick(object sender, EventArgs e)
        {
            if (ApplicationState.Tab != ShownTab.Settings) ApplicationState.Tab = ShownTab.Settings;
        }

        private void TabDestinationsClick(object sender, EventArgs e)
        {
            if (ApplicationState.Tab != ShownTab.Destinations) ApplicationState.Tab = ShownTab.Destinations;
        }

        private void TabStopsClick(object sender, EventArgs e)
        {
            if (ApplicationState.Tab != ShownTab.Stops) ApplicationState.Tab = ShownTab.Stops;
        }

        private void TabRoutesClick(object sender, EventArgs e)
        {
            if (ApplicationState.Tab != ShownTab.Routes) ApplicationState.Tab = ShownTab.Routes;
        }

        private void StatusFileClick(object sender, EventArgs e)
        {
            string filename = ApplicationState.Filename;
            if (filename == null) Program.Beep();
            else Program.OpenTextFile(filename);
        }

        /// <summary>
        /// Returns true if the user confirmed unsaved changes or there are no unsaved changes.
        /// </summary>
        bool CloseFile()
        {
            if (!ApplicationState.IsUnsaved) return true;

            DialogResult result = MessageBox.Show(Properties.Text.ASK_SaveFile, Properties.Text.MSG_FileUnsavedTitle, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                SaveClick(this, EventArgs.Empty);
                return true;
            }
            else if (result == DialogResult.No)
            {
                return true;
            }
            return false;
        }

        void LoadHof(Hof hoffile, bool isUnsaved)
        {
            ApplicationState.Filename = null;
            ApplicationState.Hof = hoffile;
            ApplicationState.Item = null;
            ApplicationState.IsUnsaved = isUnsaved;
        }

        void LoadFile(string filename)
        {
            var import = new HofReader(filename);

            try
            {
                import.ReadFile();
            }
            catch (Exception e)
            {
                import.Errors.Add(new Error(filename, 0, string.Format(Properties.Text.IMPORT_Exception, e.Message, e.TargetSite), true));
            }

            if (import.Errors.Count > 0 && new ErrorForm(import, true).ShowDialog() != DialogResult.OK)
            {
                return;
            }

            ApplicationState.Filename = filename;
            ApplicationState.Hof = import.NewHoffile;
            ApplicationState.Item = null;
            ApplicationState.IsUnsaved = false;
        }

        void SaveFile(string filename)
        {
            var export = new HofWriter(filename, ApplicationState.Hof);

            try
            {
                export.WriteFile();
            }
            catch (Exception e)
            {
                export.Errors.Add(new Error(filename, 0, string.Format(Properties.Text.EXPORT_Exception, e.Message, e.TargetSite), true));
            }

            if (export.Errors.Count > 0) new ErrorForm(export, false).ShowDialog();

            ApplicationState.Filename = filename;
            ApplicationState.IsUnsaved = false;
        }

        void ExportFile()
        {
            if (ApplicationState.Filename == null)
            {
                MessageBox.Show(Properties.Text.MSG_MustSave, Properties.Text.TITLE_Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            string filename = PathBusiness.GetTemporaryFilename();

            try
            {
                var export = new HofWriter(filename, ApplicationState.Hof);

                try
                {
                    export.WriteFile();
                }
                catch (Exception e)
                {
                    export.Errors.Add(new Error(filename, 0, string.Format(Properties.Text.EXPORT_Exception, e.Message, e.TargetSite), true));
                }

                if ((export.Errors.Count == 0 ||
                    new ErrorForm(export, true).ShowDialog() == DialogResult.OK) &&
                    MessageBox.Show(string.Format(Properties.Text.MSG_ExportWarning, PathBusiness.GetFilenameFromPath(ApplicationState.Filename)) + " " + Properties.Text.ASK_Continue,
                    Properties.Text.TITLE_Warning, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    foreach (var s in ApplicationState.Hof.Settings.VehicleDirectories)
                    {
                        if (PathBusiness.CheckIfDirectoryExists(PathBusiness.GetVehicleDirectory(s)))
                            PathBusiness.CopyFile(filename, PathBusiness.GetVehicleDirectory(s) + "\\" + PathBusiness.GetFilenameFromPath(ApplicationState.Filename));
                    }
                }
            }
            finally
            {
                PathBusiness.DeleteFile(filename);
            }
        }
    }
}
