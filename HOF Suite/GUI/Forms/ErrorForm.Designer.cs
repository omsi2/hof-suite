﻿namespace Hofsuite.GUI.Forms
{
	partial class ErrorForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErrorForm));
			this.acceptButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.openFileButton = new System.Windows.Forms.Button();
			this.listView = new System.Windows.Forms.ListView();
			this.column0 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.column1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.column2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.label = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// acceptButton
			// 
			resources.ApplyResources(this.acceptButton, "acceptButton");
			this.acceptButton.Name = "acceptButton";
			this.acceptButton.UseVisualStyleBackColor = true;
			this.acceptButton.Click += new System.EventHandler(this.AcceptButton_Click);
			// 
			// cancelButton
			// 
			resources.ApplyResources(this.cancelButton, "cancelButton");
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.UseVisualStyleBackColor = true;
			// 
			// openFileButton
			// 
			resources.ApplyResources(this.openFileButton, "openFileButton");
			this.openFileButton.Name = "openFileButton";
			this.openFileButton.UseVisualStyleBackColor = true;
			this.openFileButton.Click += new System.EventHandler(this.OpenFileButton_Click);
			// 
			// listView
			// 
			resources.ApplyResources(this.listView, "listView");
			this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.column0,
            this.column1,
            this.column2});
			this.listView.FullRowSelect = true;
			this.listView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.listView.MultiSelect = false;
			this.listView.Name = "listView";
			this.listView.UseCompatibleStateImageBehavior = false;
			this.listView.View = System.Windows.Forms.View.Details;
			// 
			// column0
			// 
			resources.ApplyResources(this.column0, "column0");
			// 
			// column1
			// 
			resources.ApplyResources(this.column1, "column1");
			// 
			// column2
			// 
			resources.ApplyResources(this.column2, "column2");
			// 
			// label
			// 
			resources.ApplyResources(this.label, "label");
			this.label.Name = "label";
			// 
			// ErrorReporter
			// 
			this.AcceptButton = this.acceptButton;
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.cancelButton;
			this.Controls.Add(this.label);
			this.Controls.Add(this.listView);
			this.Controls.Add(this.openFileButton);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.acceptButton);
			this.Name = "ErrorReporter";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button acceptButton;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button openFileButton;
		private System.Windows.Forms.ListView listView;
		private System.Windows.Forms.ColumnHeader column1;
		private System.Windows.Forms.ColumnHeader column2;
		private System.Windows.Forms.Label label;
		private System.Windows.Forms.ColumnHeader column0;
	}
}