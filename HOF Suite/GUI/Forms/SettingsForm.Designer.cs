﻿namespace Hofsuite.GUI.Forms
{
	partial class SettingsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.acceptButton = new System.Windows.Forms.Button();
            this.omsiText = new System.Windows.Forms.TextBox();
            this.omsiBrowse = new System.Windows.Forms.Button();
            this.omsiLabel = new System.Windows.Forms.Label();
            this.stringText = new System.Windows.Forms.TextBox();
            this.stringBrowse = new System.Windows.Forms.Button();
            this.stringLabel = new System.Windows.Forms.Label();
            this.tableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.omsiReset = new System.Windows.Forms.Button();
            this.stringReset = new System.Windows.Forms.Button();
            this.CheckPathWarnings = new System.Windows.Forms.CheckBox();
            this.CheckMask = new System.Windows.Forms.CheckBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.button4 = new System.Windows.Forms.Button();
            this.tableLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // acceptButton
            // 
            resources.ApplyResources(this.acceptButton, "acceptButton");
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.Accept);
            // 
            // omsiText
            // 
            resources.ApplyResources(this.omsiText, "omsiText");
            this.omsiText.Name = "omsiText";
            this.omsiText.ReadOnly = true;
            // 
            // omsiBrowse
            // 
            resources.ApplyResources(this.omsiBrowse, "omsiBrowse");
            this.omsiBrowse.Name = "omsiBrowse";
            this.omsiBrowse.UseVisualStyleBackColor = true;
            this.omsiBrowse.Click += new System.EventHandler(this.OmsiBrowse);
            // 
            // omsiLabel
            // 
            resources.ApplyResources(this.omsiLabel, "omsiLabel");
            this.omsiLabel.Name = "omsiLabel";
            // 
            // stringText
            // 
            resources.ApplyResources(this.stringText, "stringText");
            this.stringText.Name = "stringText";
            this.stringText.ReadOnly = true;
            // 
            // stringBrowse
            // 
            resources.ApplyResources(this.stringBrowse, "stringBrowse");
            this.stringBrowse.Name = "stringBrowse";
            this.stringBrowse.UseVisualStyleBackColor = true;
            this.stringBrowse.Click += new System.EventHandler(this.StringtemplateBrowse);
            // 
            // stringLabel
            // 
            resources.ApplyResources(this.stringLabel, "stringLabel");
            this.stringLabel.Name = "stringLabel";
            // 
            // tableLayout
            // 
            resources.ApplyResources(this.tableLayout, "tableLayout");
            this.tableLayout.Controls.Add(this.omsiReset, 2, 0);
            this.tableLayout.Controls.Add(this.omsiLabel, 0, 0);
            this.tableLayout.Controls.Add(this.stringBrowse, 3, 1);
            this.tableLayout.Controls.Add(this.omsiBrowse, 3, 0);
            this.tableLayout.Controls.Add(this.stringLabel, 0, 1);
            this.tableLayout.Controls.Add(this.omsiText, 1, 0);
            this.tableLayout.Controls.Add(this.stringText, 1, 1);
            this.tableLayout.Controls.Add(this.stringReset, 2, 1);
            this.tableLayout.Controls.Add(this.CheckPathWarnings, 0, 2);
            this.tableLayout.Controls.Add(this.CheckMask, 0, 3);
            this.tableLayout.Name = "tableLayout";
            // 
            // omsiReset
            // 
            resources.ApplyResources(this.omsiReset, "omsiReset");
            this.omsiReset.Image = global::Hofsuite.Properties.Resources.No;
            this.omsiReset.Name = "omsiReset";
            this.toolTip.SetToolTip(this.omsiReset, resources.GetString("omsiReset.ToolTip"));
            this.omsiReset.UseVisualStyleBackColor = true;
            this.omsiReset.Click += new System.EventHandler(this.OmsiReset);
            // 
            // stringReset
            // 
            resources.ApplyResources(this.stringReset, "stringReset");
            this.stringReset.Image = global::Hofsuite.Properties.Resources.No;
            this.stringReset.Name = "stringReset";
            this.toolTip.SetToolTip(this.stringReset, resources.GetString("stringReset.ToolTip"));
            this.stringReset.UseVisualStyleBackColor = true;
            this.stringReset.Click += new System.EventHandler(this.StringtemplateReset);
            // 
            // CheckPathWarnings
            // 
            resources.ApplyResources(this.CheckPathWarnings, "CheckPathWarnings");
            this.tableLayout.SetColumnSpan(this.CheckPathWarnings, 2);
            this.CheckPathWarnings.Name = "CheckPathWarnings";
            this.CheckPathWarnings.UseVisualStyleBackColor = true;
            // 
            // CheckMask
            // 
            resources.ApplyResources(this.CheckMask, "CheckMask");
            this.tableLayout.SetColumnSpan(this.CheckMask, 2);
            this.CheckMask.Name = "CheckMask";
            this.CheckMask.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            resources.ApplyResources(this.button4, "button4");
            this.button4.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button4.Name = "button4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.ResetAll);
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.acceptButton;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayout);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.acceptButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.tableLayout.ResumeLayout(false);
            this.tableLayout.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button acceptButton;
		private System.Windows.Forms.TextBox omsiText;
		private System.Windows.Forms.Button omsiBrowse;
		private System.Windows.Forms.Label omsiLabel;
		private System.Windows.Forms.TextBox stringText;
		private System.Windows.Forms.Button stringBrowse;
		private System.Windows.Forms.Label stringLabel;
		private System.Windows.Forms.TableLayoutPanel tableLayout;
		private System.Windows.Forms.Button stringReset;
        private System.Windows.Forms.Button omsiReset;
        private System.Windows.Forms.CheckBox CheckPathWarnings;
        private System.Windows.Forms.CheckBox CheckMask;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button button4;
    }
}