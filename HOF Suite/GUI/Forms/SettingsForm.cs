﻿using Hofsuite.Core.Business;

using System;
using System.Windows.Forms;

namespace Hofsuite.GUI.Forms
{
    partial class SettingsForm : Form
	{
		public SettingsForm()
		{
			InitializeComponent();

            omsiText.Text = PathBusiness.ValidOmsipath;
			stringText.Text = StringtemplateLoader.ValidFilename;
            CheckPathWarnings.Checked = SettingsBusiness.EnableOmsipathWarning;
            CheckMask.Checked = SettingsBusiness.MaskEnabled;
		}

        private void OmsiReset(object sender, EventArgs e)
        {
            omsiText.Text = "";
        }

        private void StringtemplateReset(object sender, EventArgs e)
        {
            stringText.Text = "";
            StringtemplateLoader.ValidFilename = "";
        }

        private void OmsiBrowse(object sender, EventArgs e)
        {
            var dialog = new FolderBrowserDialog
            {
                Description = Properties.Text.MSG_Omsipath_Select,
                RootFolder = Environment.SpecialFolder.Desktop,
                ShowNewFolderButton = false
            };

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if (PathBusiness.OmsipathIsValid(dialog.SelectedPath))
                {
                    omsiText.Text = dialog.SelectedPath;
                }
                else
                {
                    MessageBox.Show(string.Format(Properties.Text.MSG_Omsipath_NotValid, dialog.SelectedPath), Properties.Text.TITLE_Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void StringtemplateBrowse(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            string oldValidFilename = stringText.Text;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                StringtemplateLoader.ValidFilename = dialog.FileName;
                if (StringtemplateLoader.ValidFilename == "")
                {
                    MessageBox.Show(Properties.Text.MSG_Stringtemplate_NotValid, Properties.Text.TITLE_Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    StringtemplateLoader.ValidFilename = oldValidFilename;
                }

                stringText.Text = StringtemplateLoader.ValidFilename;
            }
        }

        private void Accept(object sender, EventArgs e)
        {
            PathBusiness.ValidOmsipath = omsiText.Text;

            SettingsBusiness.EnableOmsipathWarning = CheckPathWarnings.Checked;
            SettingsBusiness.MaskEnabled = CheckMask.Checked;
            Close();
        }

        private void ResetAll(object sender, EventArgs e)
        {
            SettingsBusiness.ResetSettings();

            omsiText.Text = PathBusiness.ValidOmsipath;
            stringText.Text = StringtemplateLoader.ValidFilename;
            CheckPathWarnings.Checked = SettingsBusiness.EnableOmsipathWarning;
            CheckMask.Checked = SettingsBusiness.MaskEnabled;

            MessageBox.Show(Properties.Text.MSG_SettingsRestart, Properties.Text.TITLE_Information);
        }
    }
}
