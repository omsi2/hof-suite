﻿namespace Hofsuite.GUI.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.status = new System.Windows.Forms.StatusStrip();
            this.statusFileLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusPlaceholder = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusProgramLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.tabRoutes = new System.Windows.Forms.Button();
            this.tabDestinations = new System.Windows.Forms.Button();
            this.tabStops = new System.Windows.Forms.Button();
            this.tabSettings = new System.Windows.Forms.Button();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.menuNew = new System.Windows.Forms.ToolStripMenuItem();
            this.menuImport = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSave = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExport = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.menuDownload = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSupport = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuManual = new System.Windows.Forms.ToolStripMenuItem();
            this.menuInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tabContent = new System.Windows.Forms.Panel();
            this.status.SuspendLayout();
            this.menu.SuspendLayout();
            this.tabPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // status
            // 
            this.status.BackColor = System.Drawing.SystemColors.Window;
            this.status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusFileLabel,
            this.statusPlaceholder,
            this.statusProgramLabel});
            resources.ApplyResources(this.status, "status");
            this.status.Name = "status";
            // 
            // statusFileLabel
            // 
            this.statusFileLabel.Image = global::Hofsuite.Properties.Resources.Error;
            this.statusFileLabel.IsLink = true;
            this.statusFileLabel.Name = "statusFileLabel";
            resources.ApplyResources(this.statusFileLabel, "statusFileLabel");
            this.statusFileLabel.Click += new System.EventHandler(this.StatusFileClick);
            // 
            // statusPlaceholder
            // 
            this.statusPlaceholder.Name = "statusPlaceholder";
            resources.ApplyResources(this.statusPlaceholder, "statusPlaceholder");
            this.statusPlaceholder.Spring = true;
            // 
            // statusProgramLabel
            // 
            this.statusProgramLabel.Name = "statusProgramLabel";
            resources.ApplyResources(this.statusProgramLabel, "statusProgramLabel");
            // 
            // openFileDialog
            // 
            resources.ApplyResources(this.openFileDialog, "openFileDialog");
            // 
            // saveFileDialog
            // 
            resources.ApplyResources(this.saveFileDialog, "saveFileDialog");
            // 
            // tabRoutes
            // 
            resources.ApplyResources(this.tabRoutes, "tabRoutes");
            this.tabRoutes.FlatAppearance.BorderColor = System.Drawing.SystemColors.Window;
            this.tabRoutes.FlatAppearance.BorderSize = 0;
            this.tabRoutes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.tabRoutes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.tabRoutes.Name = "tabRoutes";
            this.toolTip.SetToolTip(this.tabRoutes, resources.GetString("tabRoutes.ToolTip"));
            this.tabRoutes.UseVisualStyleBackColor = true;
            this.tabRoutes.Click += new System.EventHandler(this.TabRoutesClick);
            // 
            // tabDestinations
            // 
            resources.ApplyResources(this.tabDestinations, "tabDestinations");
            this.tabDestinations.FlatAppearance.BorderColor = System.Drawing.SystemColors.Window;
            this.tabDestinations.FlatAppearance.BorderSize = 0;
            this.tabDestinations.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.tabDestinations.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.tabDestinations.Name = "tabDestinations";
            this.toolTip.SetToolTip(this.tabDestinations, resources.GetString("tabDestinations.ToolTip"));
            this.tabDestinations.UseVisualStyleBackColor = true;
            this.tabDestinations.Click += new System.EventHandler(this.TabDestinationsClick);
            // 
            // tabStops
            // 
            resources.ApplyResources(this.tabStops, "tabStops");
            this.tabStops.FlatAppearance.BorderColor = System.Drawing.SystemColors.Window;
            this.tabStops.FlatAppearance.BorderSize = 0;
            this.tabStops.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.tabStops.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.tabStops.Name = "tabStops";
            this.toolTip.SetToolTip(this.tabStops, resources.GetString("tabStops.ToolTip"));
            this.tabStops.UseVisualStyleBackColor = true;
            this.tabStops.Click += new System.EventHandler(this.TabStopsClick);
            // 
            // tabSettings
            // 
            resources.ApplyResources(this.tabSettings, "tabSettings");
            this.tabSettings.FlatAppearance.BorderColor = System.Drawing.SystemColors.Window;
            this.tabSettings.FlatAppearance.BorderSize = 0;
            this.tabSettings.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.tabSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.tabSettings.Name = "tabSettings";
            this.toolTip.SetToolTip(this.tabSettings, resources.GetString("tabSettings.ToolTip"));
            this.tabSettings.UseVisualStyleBackColor = true;
            this.tabSettings.Click += new System.EventHandler(this.TabSettingsClick);
            // 
            // menu
            // 
            this.menu.BackColor = System.Drawing.SystemColors.Window;
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNew,
            this.menuImport,
            this.menuOpen,
            this.menuSave,
            this.menuSaveAs,
            this.menuExport,
            this.menuAbout});
            resources.ApplyResources(this.menu, "menu");
            this.menu.Name = "menu";
            // 
            // menuNew
            // 
            this.menuNew.Image = global::Hofsuite.Properties.Resources.EmptyFile;
            this.menuNew.Name = "menuNew";
            resources.ApplyResources(this.menuNew, "menuNew");
            this.menuNew.Click += new System.EventHandler(this.NewClick);
            // 
            // menuImport
            // 
            this.menuImport.Image = global::Hofsuite.Properties.Resources.Map;
            this.menuImport.Name = "menuImport";
            resources.ApplyResources(this.menuImport, "menuImport");
            this.menuImport.Click += new System.EventHandler(this.ImportClick);
            // 
            // menuOpen
            // 
            this.menuOpen.Image = global::Hofsuite.Properties.Resources.Open;
            this.menuOpen.Name = "menuOpen";
            resources.ApplyResources(this.menuOpen, "menuOpen");
            this.menuOpen.Click += new System.EventHandler(this.OpenClick);
            // 
            // menuSave
            // 
            this.menuSave.Image = global::Hofsuite.Properties.Resources.Save;
            this.menuSave.Name = "menuSave";
            resources.ApplyResources(this.menuSave, "menuSave");
            this.menuSave.Click += new System.EventHandler(this.SaveClick);
            // 
            // menuSaveAs
            // 
            this.menuSaveAs.Image = global::Hofsuite.Properties.Resources.Save;
            this.menuSaveAs.Name = "menuSaveAs";
            resources.ApplyResources(this.menuSaveAs, "menuSaveAs");
            this.menuSaveAs.Click += new System.EventHandler(this.SaveAsClick);
            // 
            // menuExport
            // 
            this.menuExport.Image = global::Hofsuite.Properties.Resources.Save;
            this.menuExport.Name = "menuExport";
            resources.ApplyResources(this.menuExport, "menuExport");
            this.menuExport.Click += new System.EventHandler(this.ExportClick);
            // 
            // menuAbout
            // 
            this.menuAbout.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuSettings,
            this.menuSeparator,
            this.menuDownload,
            this.menuSupport,
            this.toolStripSeparator1,
            this.menuManual,
            this.menuInfo});
            this.menuAbout.Name = "menuAbout";
            resources.ApplyResources(this.menuAbout, "menuAbout");
            // 
            // menuSettings
            // 
            this.menuSettings.Image = global::Hofsuite.Properties.Resources.Settings;
            this.menuSettings.Name = "menuSettings";
            resources.ApplyResources(this.menuSettings, "menuSettings");
            this.menuSettings.Click += new System.EventHandler(this.SettingsClick);
            // 
            // menuSeparator
            // 
            this.menuSeparator.Name = "menuSeparator";
            resources.ApplyResources(this.menuSeparator, "menuSeparator");
            // 
            // menuDownload
            // 
            this.menuDownload.Name = "menuDownload";
            resources.ApplyResources(this.menuDownload, "menuDownload");
            this.menuDownload.Click += new System.EventHandler(this.DownloadClick);
            // 
            // menuSupport
            // 
            this.menuSupport.Name = "menuSupport";
            resources.ApplyResources(this.menuSupport, "menuSupport");
            this.menuSupport.Click += new System.EventHandler(this.SupportClick);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // menuManual
            // 
            this.menuManual.Image = global::Hofsuite.Properties.Resources.Help;
            this.menuManual.Name = "menuManual";
            resources.ApplyResources(this.menuManual, "menuManual");
            this.menuManual.Click += new System.EventHandler(this.ManualClick);
            // 
            // menuInfo
            // 
            this.menuInfo.Image = global::Hofsuite.Properties.Resources.Info;
            this.menuInfo.Name = "menuInfo";
            resources.ApplyResources(this.menuInfo, "menuInfo");
            this.menuInfo.Click += new System.EventHandler(this.AboutClick);
            // 
            // tabPanel
            // 
            resources.ApplyResources(this.tabPanel, "tabPanel");
            this.tabPanel.Controls.Add(this.tabRoutes, 0, 4);
            this.tabPanel.Controls.Add(this.tabDestinations, 0, 2);
            this.tabPanel.Controls.Add(this.tabStops, 0, 3);
            this.tabPanel.Controls.Add(this.tabSettings, 0, 1);
            this.tabPanel.Name = "tabPanel";
            // 
            // tabContent
            // 
            resources.ApplyResources(this.tabContent, "tabContent");
            this.tabContent.Name = "tabContent";
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabContent);
            this.Controls.Add(this.tabPanel);
            this.Controls.Add(this.menu);
            this.Controls.Add(this.status);
            this.Name = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClosingEvent);
            this.status.ResumeLayout(false);
            this.status.PerformLayout();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.tabPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip status;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripStatusLabel statusFileLabel;
        private System.Windows.Forms.ToolStripStatusLabel statusPlaceholder;
        private System.Windows.Forms.ToolStripStatusLabel statusProgramLabel;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem menuNew;
        private System.Windows.Forms.ToolStripMenuItem menuImport;
        private System.Windows.Forms.ToolStripMenuItem menuOpen;
        private System.Windows.Forms.ToolStripMenuItem menuSave;
        private System.Windows.Forms.ToolStripMenuItem menuSaveAs;
        private System.Windows.Forms.ToolStripMenuItem menuExport;
        private System.Windows.Forms.ToolStripMenuItem menuAbout;
        private System.Windows.Forms.ToolStripMenuItem menuSettings;
        private System.Windows.Forms.ToolStripSeparator menuSeparator;
        private System.Windows.Forms.ToolStripMenuItem menuManual;
        private System.Windows.Forms.ToolStripMenuItem menuInfo;
        private System.Windows.Forms.TableLayoutPanel tabPanel;
        private System.Windows.Forms.Button tabRoutes;
        private System.Windows.Forms.Button tabDestinations;
        private System.Windows.Forms.Button tabStops;
        private System.Windows.Forms.Button tabSettings;
        private System.Windows.Forms.ToolStripMenuItem menuDownload;
        private System.Windows.Forms.ToolStripMenuItem menuSupport;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Panel tabContent;
    }
}