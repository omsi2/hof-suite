﻿using Hofsuite.Core;
using Hofsuite.Core.Structs;

using System;
using System.Windows.Forms;

namespace Hofsuite.GUI.Forms
{
    partial class ErrorForm : Form
	{
		public ErrorForm(IHasErrors errorProvider, bool allowCancel)
		{
			InitializeComponent();
			foreach (var item in errorProvider.Errors) listView.Items.Add(item);
			cancelButton.Enabled = allowCancel;
		}

		private void AcceptButton_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.OK;
			Close();
		}

		private void OpenFileButton_Click(object sender, EventArgs e)
		{
			try
			{
				Program.OpenTextFile((listView.SelectedItems[0] as Error).File);
			}
			catch (Exception)
			{
                Program.Beep();
			}
		}
	}
}
